extends Area2D

onready var id = str (get_tree().get_network_unique_id())

var puzzlename = "nothing";
var puzzle;
var inrange = false;
var justpressed = true;
var load3d;
var loader;
var progress;
var wait_frames;
var time_max = 60000;

func _ready():
	load3d = load("res://LoadScreen/Loading3D.tscn")

func update_progress():
	progress = float(loader.get_stage()) / loader.get_stage_count();

func set_new_scene(scene_resource):
	get_node("/root/world").show();
	get_node("/root/Loading3D").queue_free();
	puzzle = scene_resource.instance();
	get_node("/root/world").add_child(puzzle)
	get_node("../../StageMusic").stop()
	get_tree().connect("node_removed", self, "startmusic")
	gamestate.get_ClientPlayer().num_classes += 1;
	if("rhythm" in puzzlename) == false:
		gamestate.get_ClientPlayer().updateVitals({
			health = 0,
			sanity = -15,
		});

func _process(delta):
	if loader == null:
		set_process(false);
		return
	
	if(wait_frames > 0):
		wait_frames -=1;
		return;
	
	var t = OS.get_ticks_msec();
	while OS.get_ticks_msec() < t + time_max:
		var err = loader.poll();
		if err == ERR_FILE_EOF:
			var resource = loader.get_resource()
			loader = null
			set_new_scene(resource)
			break;
		elif err == OK:
			update_progress();
		else:
			show_error()
			loader = null;
			break;

func _input(event):
	if(Input.is_action_pressed("select_item") && inrange == true && justpressed == false):
		if(gamestate.get_ClientPlayer().vitals["sanity"] < 15):
			gamestate.get_ClientPlayer().openBubble("\nYou cannot go to class, you are too stressed out.\n\nRestore sanity first!\n");
			return;
		gamestate.in_world = false;
		justpressed = true
		loader = ResourceLoader.load_interactive(puzzlename);
		if loader == null:
			show_error();
			return
		set_process(true)
		get_node("/root/world").hide();
		get_node("/root").add_child(load3d.instance());
		wait_frames = 60;

func startmusic(node):
	if(node == puzzle):
		gamestate.in_world = true;
		get_node("../../StageMusic").play()
		get_tree().disconnect("node_removed", self, "startmusic")

func _on_Area2D_area_entered(body):
	#print(area.get_name())
	puzzlename = "res://puzzles/rhythm_veeex.tscn"
	body_entered(body)

func body_entered(body):
	if (body.get_name() == id) and gamestate.get_ClientPlayer().isPlayerGraduated == false:
		inrange = true
	
	justpressed = false

func _on_Area2D_area_exited(body):
	if (body.get_name() == id):
		inrange = false

func _on_LeadAcid_area_entered(body):
	#print(area.get_name())
	puzzlename = "res://puzzles/LeadAcid/rhythm_leadacid.tscn"
	body_entered(body)

func _on_LeadAcid_area_exited(body):
	inrange = false

func _on_SudokuArea_entered(body):
	#print(area.get_name())
	puzzlename = "res://puzzles/Sudoku/SudokuTest.tscn"
	body_entered(body)

func _on_StumpArea_entered(body):
	puzzlename = "res://puzzles/Stump/Stump.tscn"
	body_entered(body)

func _on_SudokuArea_exited(body):
	inrange = false

func _on_K_body_entered(body):
	puzzlename = "res://puzzles/rhythm_k.tscn"
	body_entered(body)

func _on_RelentlessTrigger_body_entered(body):
	puzzlename = "res://puzzles/Relentless-Big/Relentless-Big.tscn"
	body_entered(body)
