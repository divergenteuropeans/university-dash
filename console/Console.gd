extends 'IConsole.gd'


### Console nodes
onready var _consoleBox = $ConsoleBox
onready var _consoleText = $ConsoleBox/Container/ConsoleText
onready var _scoreboardBox = $ScoreboardBox
onready var _scoreboardText = $ScoreboardBox/Container/ScoreboardText
onready var _consoleLine = $ConsoleBox/Container/LineEdit
onready var _animationPlayer = $ConsoleBox/AnimationPlayer
onready var _animationPlayerSc = $ScoreboardBox/AnimationPlayer

func _init():
	# Used to clear text from bb tags
	_eraseTrash = RegExLib.get('console.eraseTrash')


func _ready():
	# Allow selecting console text
	_consoleText.set_selection_enabled(true)
	# Follow console output (for scrolling)
	_consoleText.set_scroll_follow(true)
	# React to clicks on console urls
	_consoleText.connect('meta_clicked', self, '_handleUrlClick')

	# Hide console by default
	_consoleBox.hide()
	_scoreboardBox.hide()
	_animationPlayer.connect("animation_finished", self, "_toggleAnimationFinished")
	toggleConsole()

	# Console keyboard control
	set_process_input(true)

	_consoleLine.connect('text_entered', self, '_handleEnteredCommand')

	# By default we show help
	var v = Engine.get_version_info()

	#writeLine(\
	#	ProjectSettings.get_setting("application/config/name") + \
	#	" (Godot " + str(v.major) + '.' + str(v.minor) + '.' + str(v.patch) + ' ' + v.status+")\n" + \
	#	"Type [color=#ffff66][url=help]help[/url][/color] to get more information about usage")


	# Init base commands
	BaseCommands.new()


# @param  Event  e
func _input(e):
	if isConsoleShown and Input.is_action_just_pressed('godot_console_open') and has_node("/root/world"):
		openConsole()
	if Input.is_action_just_pressed('godot_console_close'):
		closeConsole()

	if Input.is_action_just_pressed('scoreboard_open') and has_node("/root/world"):
		openScoreboard()
	if Input.is_action_just_released('scoreboard_open'):
		_scoreboardBox.hide()

	# Show prev line in history
	if Input.is_action_just_pressed(action_history_up):
		_currCmd = _History.prev()

		if _currCmdHandler == null:
			_currCmdHandler = _consoleLine.text

	# Show next line in history
	if Input.is_action_just_pressed(action_history_down):
		_currCmd = _History.next()

		if !_currCmd and _currCmdHandler != null:
			_currCmd = _currCmdHandler
			_currCmdHandler = null

	# Autocomplete on TAB
	if _consoleLine.text and _consoleLine.has_focus() and Input.is_key_pressed(KEY_TAB):
		if !_Commands.Autocomplete._filtered.has(_consoleLine.text):
			_currCmdHandler = _consoleLine.text
			_Commands.Autocomplete.reset()

		_Commands.Autocomplete.filter(_currCmdHandler)
		_currCmd = _Commands.Autocomplete.next()

	# Finish
	if _currCmd != null:
		_setConsoleLine(_currCmd)
		_currCmd = null
		_consoleLine.accept_event()


# @param  string  command
func _handleEnteredCommand(command):  # void
	if command.empty():
		return

	if !command.begins_with('/'):
		rpc("gameSay", "<" + gamestate.get_player_name() + "> " + command)
		_consoleLine.clear()
		var players = get_node("/root/world/players")
		var id = str (get_tree().get_network_unique_id())	# Get User ID
		players.get_node(id).openBubble(command)	# Display Chat Bubble
		return
	else:
		command = command.right(1)

	# Some preparations
	_History.reset()
	_Commands.Autocomplete.reset()
	command = _eraseTrash.sub(command, '', true)

	# Get command name
	var cmdName = command.split(' ', false)
	if typeof(cmdName) != TYPE_STRING_ARRAY:
		Log.warn('Could not get command name', \
			'Console: _handleEnteredCommand')
		return
	cmdName = cmdName[0]

	var Command = _Commands.get(cmdName)

	if !Command:
		Log.warn('No such command', \
			'Console: _handleEnteredCommand')
		_consoleLine.clear()
		return

	# Get args
	var args = []
	var cmdArgs = null
	if Command.requireArgs():
		cmdArgs = command.substr(cmdName.length() + 1, command.length())

		if Command._target._type == Console.Callback.VARIABLE or Command._arguments.size() == 1:
			args.append(cmdArgs)
		elif Command.requireStrings():

			var isString = null
			var prevDelimiter = 0

			var i = 0
			while Command.requireArgs() != args.size():
				if cmdArgs[i] == '"' or cmdArgs[i] == "'":
					if !isString:
						isString = cmdArgs[i]
					elif cmdArgs[i] == isString and cmdArgs[i - 1 if i > 0 else 0] != '\\':
						isString = null

				if !isString:
					if prevDelimiter and (cmdArgs[i] == '"' or cmdArgs[i] == "'"):
						args.append(cmdArgs.substr(prevDelimiter + 1, i - 3).replace('\\' + \
							str(cmdArgs[prevDelimiter]), cmdArgs[prevDelimiter]))
						prevDelimiter = i + 1
					elif cmdArgs[i] == ' ' and cmdArgs[i + 1 if i < cmdArgs.length() - 1 else i] != ' ':
						args.append(cmdArgs.substr(prevDelimiter, i))
						prevDelimiter = i + 1

				i += 1

		else:
			args = cmdArgs.split(' ', false)

	# Execute
	var finCommand = Command._alias
	if cmdArgs:
		finCommand += ' ' + cmdArgs

	_History.push(finCommand)
	writeLine('[color=#999999]$[/color] ' + finCommand)
	Command.run(args)
	_consoleLine.clear()


# @param  string  url
func _handleUrlClick(url):  # void
	_setConsoleLine(url + ' ')

# @param  string  text
# @param  bool    moveCaretToEnd
func _setConsoleLine(text, moveCaretToEnd = true):  # void
	_consoleLine.text = text
	_consoleLine.grab_focus()

	if moveCaretToEnd:
		_consoleLine.caret_position = text.length()


# @param  string      alias
# @param  Dictionary  params
func register(alias, params):  # int
	return _Commands.register(alias, params)


# @param  string  alias
func unregister(alias):  # int
	return _Commands.unregister(alias)

# @param  string  message
func write(message):  # void
	message = str(message)
	print(_eraseTrash.sub(message, '', true))
	_consoleText.set_bbcode(_consoleText.get_bbcode() + message)


# @param  string  message
func writeLine(message = ''):  # void
	message = str(message)
	print(_eraseTrash.sub(message, '', true))
	_consoleText.set_bbcode(_consoleText.get_bbcode() + message + '\n')

# Print a game message said by a player
sync func gameSay(message): # void
	writeLine(message)

func openConsole():
	if  isConsoleShown:
		#get_tree().paused = true
		_consoleBox.show()
		_consoleLine.clear()
		_consoleLine.grab_focus()
		_animationPlayer.play_backwards('fade')
	isConsoleShown = !isConsoleShown

func closeConsole():
	#get_tree().paused = false
	_animationPlayer.play('fade')
	isConsoleShown = !isConsoleShown

### Main Scoreboard Code ###

func openScoreboard():
	get_node("ScoreboardBox/ItemList").clear()
	var name
	var scores = {} # in format p_id : score
	var playerslist = gamestate.get_player_list()
	
	for p_id in gamestate.player_IDs:
		var yourscore = 0
		for n in playersStats[p_id].values():
			yourscore += n 
		scores[p_id] = yourscore
		var maxScore = 0
		for p_id in scores:
			if maxScore < scores[p_id]:
				maxScore = scores[p_id]
		# To avoid division by 0 cases (at the start of the game).
		var yourGPA = 0
		var localscore = 0
		for n in gamestate.get_ClientPlayer().getStats().values():	# Manually sum Skills earned
			localscore += n
		if maxScore > 0:
			yourGPA = ((localscore * 4.00) / (maxScore))
		gamestate.get_ClientPlayer().GPA = yourGPA
		if (p_id == get_tree().get_network_unique_id()):
			name = "(You)  " + gamestate.get_player_name()
		else:
			name = playersNames[p_id]
		get_node("ScoreboardBox/ItemList").add_item(name+" —"+
		"  Cr: " + str(playersStats[p_id]["creativity"]) +
		"  L: " + str(playersStats[p_id]["logic"]) +
		"  Co: " + str(playersStats[p_id]["communication"]) +
		"  I: " + str(playersStats[p_id]["initiative"]) +
		"  H: " + str(playersStats[p_id]["humanity"]) +
		"  V: " + str(playersStats[p_id]["vocational"]))
	_scoreboardBox.show()

var playersStats = {}
var playersNames = {}
var playersScore = {}
remote func scoreUpdate(id, name, skills, score):
	for p_id in gamestate.players:
		if (get_tree().is_network_server()) and (p_id != id):	#If Host, Tell Guests
			rpc_id(p_id, "scoreUpdate",id, name, skills, score)
		elif (id == get_tree().get_network_unique_id()):		#If Guest Making Call, Tell Host
			rpc_id(1, "scoreUpdate",id, name, skills, score)
	
	playersStats[id] = skills
	playersNames[id] = name


	#Create Ordered List of Players
#	var temp = Array()
#	for key in playersScore:
#		 temp.append(key)
#	#Reverse List so low scores are on bottom
#	var backwards = Array()
#	for i in range (len(temp), 0):
#		backwards.append(i)
#	listOrder = backwards

#	var players = get_node("/root/world/players")
#	var child = players.get_children()
#	for p_id in child:
#		playersStats[child[p_id].get_name()] = child[p_id].getStats()


#	for p_id in gamestate.players:
#		if (p_id == get_tree().get_network_unique_id()):
#			playersStats[id] = skills
#		else:
#			rpc_id(p_id, "scoreUpdate", skills, id)

##Manually update() ditionary because GDScript does not support it
#	var newEntry = false
#	for key in playersStats:	# Check if entry exists and Update
#		if key == int(id):
#			playersStats[key] = skills
#			newEntry = true
#	if newEntry == false:

func _toggleAnimationFinished(animation):  # void
	if isConsoleShown:
		_consoleBox.hide()
