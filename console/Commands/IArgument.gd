
extends Object


enum ARGASSIG \
{
	CANCELED = 2
}


# @var  string
var _name

# @var  BaseType
var _type

# @var  Variant
var value = null setget setValue, getValue


# @param  Variant  inValue
func setValue(inValue):  # int
	pass


func getValue():  # Variant
	pass
