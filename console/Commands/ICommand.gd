
extends Object


# @var  string
var _alias

# @var  Callback
var _target

# @var  Array<Argument>
var _arguments = []

# @var  string|null
var _description


# @param  Array  inArgs
func run(inArgs):  # int
	pass


func describe():  # void
	pass


func requireArgs():  # int
	pass


func requireStrings():  # bool
	pass
