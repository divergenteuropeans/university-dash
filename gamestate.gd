extends Node

# Default game port
const DEFAULT_PORT = 10567

# Max number of players
const MAX_PEERS = 5

# Name for my player
var player_name = "name"	# String is replaced
var player_skin = "res://player/charwalks/boy/charwalk_boy_blondehair_blueshirt.png"

# Names for remote players in id:name format
var players = {}		# Contains OTHER players, not Self
var players_skin = {}
var player_IDs = Array()# Contains everybody

# Number of frames to wait for loading screen. World is large and (especially on
# disk) can take forever to load, this is more entertaining and less freaky than
# a frozen screen.
var wait_frames = 60;
var time_max = 60000;

# In world. Determines whether the player is actually in the world, or in a menu
var in_world = false;

# Scene loader object. Used for multi-threaded world loading
var loader;
var spawns;

# Ideally, Gamestate would only be called after Pressing Join
# and console after starting a game session.
# Since this requires a great deal of refactoring, cleanup() is used
func cleanup():
	players = {}
	players_skin = {}
	player_IDs = Array()
	Console.playersStats = {}
	Console.playersNames = {}
	Console.playersScore = {}

# Signals to let lobby GUI know what's going on
signal player_list_changed()
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(what)

# Callback from SceneTree
func _player_connected(id):
	# This is not used in this demo, because _connected_ok is called for clients
	# on success and will do the job.
	pass

# Callback from SceneTree
func _player_disconnected(id):
	if (get_tree().is_network_server()):
		if (has_node("/root/world")): # Game is in progress
			emit_signal("game_error", "Player " + players[id] + " disconnected")
			end_game()
		else: # Game is not in progress
			# If we are the server, send to the new dude all the already registered players
			unregister_player(id)
			for p_id in players:
				# Erase in the server
				rpc_id(p_id, "unregister_player", id)

# Callback from SceneTree, only for clients (not server)
func _connected_ok():
	# Registration of a client beings here, tell everyone that we are here
	rpc("register_player", get_tree().get_network_unique_id(), player_name, player_skin)
	emit_signal("connection_succeeded")

# Callback from SceneTree, only for clients (not server)
func _server_disconnected():
	emit_signal("game_error", "Server disconnected")
	end_game()

# Callback from SceneTree, only for clients (not server)
func _connected_fail():
	get_tree().set_network_peer(null) # Remove peer
	emit_signal("connection_failed")

# Savegame functions

var SAVEGAME_LOCATION = "user://save.bin"

func check_savegame_exists():
	return File.new().file_exists(SAVEGAME_LOCATION)

func save():
	var save_data = {
		
	}
	return save_data
	
func save_game():
	var savegame = File.new()
	savegame.open(SAVEGAME_LOCATION, File.WRITE)
	
	#var players = get_node("/root/world/players").get_children()
	
	#for i in players:
	#	savegame.store_line(to_json(i))
	#savegame.close()
	
	savegame.store_line(to_json(save()))
	
	return

func load_game():
	return

# End savegame functions

# Lobby management functions

remote func register_player(id, new_player_name, new_player_skin):
	if (get_tree().is_network_server()):
		# If we are the server, let everyone know about the new player
		rpc_id(id, "register_player", 1, player_name, player_skin) # Send myself to new dude
		for p_id in players: # Then, for each remote player
			rpc_id(id, "register_player", p_id, players[p_id], players_skin[p_id]) # Send players to new dude
			rpc_id(p_id, "register_player", id, new_player_name, new_player_skin) # Send new dude to players

	players[id] = new_player_name
	players_skin[id] = new_player_skin
	emit_signal("player_list_changed")

remote func unregister_player(id):
	players.erase(id)
	emit_signal("player_list_changed")

remote func pre_start_game(spawn_points):
	spawns = spawn_points;
	loader = ResourceLoader.load_interactive("res://world.tscn");
	if loader == null:
		show_error();
		return
	set_process(true)
	get_tree().get_root().get_node("MainMenu").hide();
	get_tree().get_root().get_node("lobby").hide();
	get_node("/root").add_child(load("res://LoadScreen/Loading3D.tscn").instance());
	wait_frames = 200;
#	var world = load("res://world.tscn").instance()
#	world.hide();
#	get_tree().get_root().add_child(world)

func _process(delta):
	if loader == null:
		set_process(false);
		return
	
	if(wait_frames > 0):
		wait_frames -=1;
		return;
	
	var t = OS.get_ticks_msec();
	while OS.get_ticks_msec() < t + time_max:
		var err = loader.poll();
		if err == ERR_FILE_EOF:
			var resource = loader.get_resource()
			loader = null
			set_new_scene(resource)
			break;
		elif err == OK:
			pass
		else:
			show_error()
			loader = null;
			break;

func set_new_scene(resource):
	get_tree().get_root().add_child(resource.instance());
	get_tree().get_root().get_node("MainMenu/Music").stop()
	var player_scene = load("res://player/player.tscn")
	var world = get_node("/root/world");

	for p_id in spawns:
		var spawn_pos = world.get_node("spawn_points/" + str(spawns[p_id])).position
		var player = player_scene.instance()

		player.set_name(str(p_id)) # Use unique ID as node name
		player.position=spawn_pos
		player.set_network_master(p_id) #set unique id as master
		player_IDs.append(p_id)

		if (p_id == get_tree().get_network_unique_id()):
			# If node for this peer id, set name
			player.set_player_name(player_name)
			player.set_player_skin(player_skin) 		# client
			player.get_node("Camera2D").make_current()	# initialize cams client-side
		else:
			# Otherwise set name from peer
			player.set_player_name(players[p_id])		 # guest
			player.set_player_skin(players_skin[p_id])
		world.get_node("players").add_child(player)

	# Set up score
	world.get_node("score").add_player(get_tree().get_network_unique_id(), player_name)
	for pn in players:
		world.get_node("score").add_player(pn, players[pn])

	if (not get_tree().is_network_server()):
		# Tell server we are ready to start
		rpc_id(1, "ready_to_start", get_tree().get_network_unique_id())
	elif players.size() == 0:
		post_start_game();

remote func post_start_game():
	var world = get_node("/root/world")
	#Start Character Creation
	var menu = 	load("res://title_menu/Skills.tscn")
	get_node("/root/Loading3D").queue_free();
	menu = menu.instance()
	world.add_child(menu)
	world.get_node("StageMusic").stop();
	world.show();
	
	get_tree().set_pause(false) # Unpause and unleash the game!
	world.letThereBeLight()


var players_ready = []

remote func ready_to_start(id):
	assert(get_tree().is_network_server())

	if (not id in players_ready):
		players_ready.append(id)

	if (players_ready.size() == players.size()):
		for p in players:
			rpc_id(p, "post_start_game")
		post_start_game()

func host_game(new_player_name, new_player_skin):
	player_name = new_player_name
	player_skin = new_player_skin
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(host)

func join_game(ip, new_player_name, new_player_skin):
	player_name = new_player_name
	player_skin = new_player_skin
	var host = NetworkedMultiplayerENet.new()
	host.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(host)

func get_player_list():
	return players.values()

func get_player_name():
	return player_name
	
func get_player_skin():
	return player_skin

	## Chat Controls
#func openBubble(msg):
#	var players = get_node("/root/world/players")
#	var id = str (get_tree().get_network_unique_id())	# Get User ID
#	players.get_node(id).openBubble(msg)	# Instance-Node Name = UID


func begin_game():
	assert(get_tree().is_network_server())

	# Create a dictionary with peer id and respective spawn points, could be improved by randomizing
	var spawn_points = {}
	spawn_points[1] = 0 # Server in spawn point 0
	var spawn_point_idx = 1
	for p in players:
		spawn_points[p] = spawn_point_idx
		spawn_point_idx += 1
	# Call to pre-start game with the spawn points
	for p in players:
		rpc_id(p, "pre_start_game", spawn_points)
	
	pre_start_game(spawn_points)

func end_game():
	if (has_node("/root/world")): # Game is in progress
		# End it
		get_node("/root/world").queue_free()
		cleanup()

	emit_signal("game_ended")
	players.clear()
	get_tree().set_network_peer(null) # End networking

func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")


func get_ClientPlayer():
	var player = get_node("/root/world/players").get_node(str (get_tree().get_network_unique_id()))
	return player