extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var item
func _ready():
	refresh()
	pass
func refresh():
	for counter in self.get_child_count():
		get_child(counter).queue_free()
		
	randomize()
	var item_count = randi()%90
	while(item_count > 0):
		randomize()
		var itemChoice = randi()%3
#		print(itemChoice)
		if(itemChoice == 1 || itemChoice == 0):
			item = load("res://objects/Consumables/simple_food.tscn").instance()
		if(itemChoice == 2):
			item = load("res://objects/Consumables/quality_food.tscn").instance()
		if(itemChoice == 3):
			item = load("res://objects/Consumables/energy_drink.tscn").instance()
		#item.set_position(Vector2(rand_range(-9180, 7630),rand_range(-4530, 7637)))
		item_count -= 1
		randomize()
		var position_choice = randi()%7
		if(position_choice == 0):
			item.set_position(Vector2(rand_range(-3790, -1920),rand_range(2560, 4270)))
		elif(position_choice == 1):
			item.set_position(Vector2(rand_range(-1680, 1200),rand_range(1350, 2300)))
		elif(position_choice == 2):
			item.set_position(Vector2(rand_range(3170, 5470),rand_range(570, 1630)))
		elif(position_choice == 3):
			item.set_position(Vector2(rand_range(3170, 4080),rand_range(2960, 5660)))
		elif(position_choice == 4):
			item.set_position(Vector2(rand_range(-2930, 1050),rand_range(-2160, -1680)))
		elif(position_choice == 5):
			item.set_position(Vector2(rand_range(-2930, -2260),rand_range(-2720, -2250)))
		elif(position_choice == 6):
			item.set_position(Vector2(rand_range(-8680, -4080),rand_range(4420, 5590)))
		elif(position_choice == 7):
			item.set_position(Vector2(rand_range(-6045, -4510),rand_range(5620, 6910)))
		self.add_child(item)