extends Spatial

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var animplayer

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	animplayer = get_node("AnimationPlayer");
	animplayer.play("RollIn");
	pass

func _process(delta):
	if (animplayer.is_playing() == false) or (Input.is_action_pressed("select_item")):
		get_tree().change_scene("res://title_menu/MainMenu.tscn");

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
