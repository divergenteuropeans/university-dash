extends Node2D

# Object Primatives
onready var _lightbulb = get_node("LightBulb")

sync func letThereBeLight():
	phase_day()

## GAME TIMER ##
var phase_name = "Day"
var day_counter = -1
var timer
var time = {
"time_day":240,		# 4 Minutes
"time_evening":360,	# 6 Minutes
"time_night":30,	# 30 Seconds
}

func _ready():
	timer = get_node("phase-timer");

sync func phase_day():
	timer.disconnect("timeout", self, "phase_day");
	_lightbulb.play("toDay")
	day_counter += 1
	DisplayDays()
	phase_name = "Day"
	timer.set_wait_time(time["time_day"])
	timer.connect("timeout", self, "phase_evening")
	timer.start()
	get_node("item_container").refresh()
sync func phase_evening():
	timer.disconnect("timeout", self, "phase_evening");
	_lightbulb.play("toEvening")
	phase_name = "Evening"
	timer.set_wait_time(time["time_evening"])
	timer.connect("timeout", self, "phase_night")
	timer.start()
	get_node("item_container").refresh()
sync func phase_night():
	timer.disconnect("timeout", self, "phase_night");
	_lightbulb.play("toNight")
	phase_name = "Night"
	timer.set_wait_time(time["time_night"])
	timer.connect("timeout", self, "phase_day")
	timer.start()
	get_node("item_container").refresh()
func get_time():	#called every frame
	return timer.get_time_left()
func get_phase():	#called every frame
	return phase_name

func DisplayDays():
	var players = gamestate.get_ClientPlayer()
	players.updateDay(day_counter)
