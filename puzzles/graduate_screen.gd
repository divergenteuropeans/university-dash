extends CanvasLayer

# A couple widgets we may want to craft custom graduate screens
var Graduation_Message_Widget;
var Place_Number_Widget;

func _ready():
	Graduation_Message_Widget = get_node("Stats_Panel/HBoxContainer/StatLabel/Message");
	Place_Number_Widget       = get_node("Stats_Panel/SkillPool/unspent");

func _on_Button_pressed():
	gamestate.get_ClientPlayer().isPlayerGraduated = true;
	self.queue_free();