extends AnimationPlayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var currentnode;

# Named in such a way to suggest a boolean, actually an integer used to track
# position in the song.
var isplaying;

# These two are used simply to track which notes have a selection. This means
# that programming a song can be just setting tiles in the Notes map.
var notesnode;
var usednotes;

var currentpos;

var currentscore;
var finalscore;

var readySetGo;
var cheers;

var fkey;
var gkey;
var hkey;
var jkey;

var fanimation;
var ganimation;
var hanimation;
var janimation;

var tutorializer_played = false;
var tutorializer_page = 0;
var tutorializer_text = [
	"Welcome to the Rhythm Game!\n\nHere you'll get to play along with a song, in order to gain some creative skills.\n\nFirst of all, you should familiarize yourself with the [F], [G], [H], and [J] keys. These\nare like your fingers on the fretboard, when you press a key it hammers a note out\non the virtual score in front of you.",
	"The goal of the game is to hit as many of the notes as possible.\n\nDon't worry though, there's no penalty for missing them! This game\nis meant to be easy to play, but hard to master. If you hit a key without a note,\nhowever, you'll lose points, so make sure that your timing is close!\n\nIf you hit a note just before or just after it plays, there's no need to fret! (See what we\ndid there?) You'll get one point instead of three, but if you hold down the key\njust right you can get up to seven points per note!\n\nHere's a breakdown of how that works:\n\nOne tile early or late: +1 point\nExactly on-time: +5 points\nMiss: -3 points\n\n\nIf you haven't played before, it's probably best to just tap the keys quickly and gently, and\ntry to just hit the note.",
	"While this game really focuses on honing your character's creative skills, it also doesn't\nimpact their sanity as much as the others. Try to balance when you play it though,\nbecause this game takes a set amount of time to play each time you play it.",
	"When you're ready to try it, press Next and a song will start!\n\nJust remember: there's no need to hit every note! Relax and enjoy the music,\nand hope for the best.\n\nIn this song, you won't get anywhere near 500 points, the song is too short.\nDon't panic if you think the score looks low!\n\nGood luck!",
	"That's it! You can find the rhythm game in various different places\naround the map."];

func _ready():
	set_process_unhandled_input(true);
	# Initialization here
	currentnode  = get_node("ScrollingMap");
	notesnode    = get_node("ScrollingMap/Notes")
	usednotes    = notesnode.get_used_cells();
	isplaying    = 16;
	currentscore = 0;
	currentpos   = 31;
	readySetGo   = get_node("ReadySetGo");
	cheers       = get_node("Cheers");
	fkey         = false;
	gkey         = false;
	hkey         = false;
	jkey         = false;
	fanimation   = get_node("../../FAnimation");
	ganimation   = get_node("../../GAnimation");
	hanimation   = get_node("../../HAnimation");
	janimation   = get_node("../../JAnimation");
	tutorializer_next();
#	print("running script OK");

func _input(event):
	var got = false;
	var key = false;
	if("InputEventMouseButton" in event.as_text()) == false && tutorializer_played == true:
		if("F" in event.as_text()):
			key = true;
			if(fkey == false):
				fkey = true;
				if(Vector2(4, currentpos-1) in usednotes):
					currentscore+=1
					got = true;
					if(!fanimation.is_playing()):
						fanimation.play("BlowUp");
				elif(Vector2(4, currentpos) in usednotes):
					currentscore+=5;
					got = true;
					if(!fanimation.is_playing()):
						fanimation.play("BlowUp-Green");
				elif(Vector2(4, currentpos+1) in usednotes):
					currentscore+=1
					got = true;
					if(!fanimation.is_playing()):
						fanimation.play("BlowUp");
				else:
					if(!fanimation.is_playing()):
						fanimation.play("BlowUp-Grey");
		if("G" in event.as_text()):
			key = true;
			if(Vector2(5, currentpos-1) in usednotes):
				if(gkey == false):
					currentscore+=1
					if(!ganimation.is_playing()):
						ganimation.play("BlowUp");
				got = true;
			elif(Vector2(5, currentpos) in usednotes):
				if(gkey == false):
					currentscore+=5;
					if(!ganimation.is_playing()):
						ganimation.play("BlowUp-Green");
				got = true;
			elif(Vector2(5, currentpos+1) in usednotes):
				if(gkey == false):
					currentscore+=1
					if(!ganimation.is_playing()):
						ganimation.play("BlowUp");
				got = true;
			else:
				if(gkey == false):
					if(!ganimation.is_playing()):
						ganimation.play("BlowUp-Grey");
		if("H" in event.as_text()):
			key = true;
			if(hkey == false):
				hkey = true;
				if(Vector2(6, currentpos-1) in usednotes):
					currentscore+=1
					if(!hanimation.is_playing()):
						hanimation.play("BlowUp");
					got = true;
				elif(Vector2(6, currentpos) in usednotes):
					currentscore+=5;
					if(!hanimation.is_playing()):
						hanimation.play("BlowUp-Green");
					got = true;
				elif(Vector2(6, currentpos+1) in usednotes):
					currentscore+=1
					if(!hanimation.is_playing()):
						hanimation.play("BlowUp");
					got = true;
				else:
					if(!hanimation.is_playing()):
						hanimation.play("BlowUp-Grey");
		if("J" in event.as_text()):
			key = true;
			if(jkey == false):
				jkey = true;
				if(Vector2(7, currentpos-1) in usednotes):
					currentscore+=1
					if(!janimation.is_playing()):
						janimation.play("BlowUp");
					got = true;
				elif(Vector2(7, currentpos) in usednotes):
					currentscore+=5;
					if(!janimation.is_playing()):
						janimation.play("BlowUp-Green");
					got = true;
				elif(Vector2(7, currentpos+1) in usednotes):
					currentscore+=1
					if(!janimation.is_playing()):
						janimation.play("BlowUp");
					got = true;
				else:
					if(!janimation.is_playing()):
						janimation.play("BlowUp-Grey");
		if(!got && key):
			currentscore -= 3;
		fkey = "F" in event.as_text();
		gkey = "G" in event.as_text();
		hkey = "H" in event.as_text();
		jkey = "J" in event.as_text();

func tutorializer_startrhythm():
	startscroll();

func startscroll():
	get_node("../AnimationPlayer2").play("Drift");
	get_node(".").play("New Anim");

func scrollpage():
	if(isplaying==0):
		get_node("AudioStreamPlayer").play();
		get_node("ScoreLabel").show();
	if(isplaying==8):
		readySetGo.text = "Set!";
	if(isplaying==4):
		readySetGo.text = "Go!";
	if(isplaying <= 0):
		readySetGo.text = str(currentscore);
		if(currentscore < 20):
			cheers.text = "Let's Go!"
		elif(currentscore < 50):
			cheers.text = "Good Start!";
		elif(currentscore < 200):
			cheers.text = "Nice!";
		elif(currentscore < 800):
			cheers.text = "Great!";
		elif(currentscore < 1000):
			cheers.text = "Awesome!";
		elif(currentscore < 1500):
			cheers.text = "Simply Amazing!";
		elif(currentscore > 1500):
			cheers.text = "LEGENDARY.";
	isplaying  -=1;
	currentpos -=1;
	fkey = false;
	gkey = false;
	hkey = false;
	jkey = false;
	currentnode.global_translate(Vector2(0,48))
	if(Vector2(-1, currentpos-1) in usednotes):
		get_node(".").stop(false);
		get_node("../AnimationPlayer2").stop(false);
		get_node("Backbutton").show();
		tutorializer_next();
		finalscore = currentscore;

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_Backbutton_pressed():
#	var tree_root = get_tree().get_root();
	get_node("../../..").queue_free();


func tutorializer_next():
	get_node("../../Tutorializer").show();
	if(tutorializer_page <= tutorializer_text.size()-1):
		if tutorializer_page == 4:
			if tutorializer_played == false:
				get_node("../../Tutorializer").hide();
				startscroll();
				tutorializer_played = true;
		get_node("../../Tutorializer/Tutorial-Text").text = tutorializer_text[tutorializer_page];
		tutorializer_page += 1;
	else:
		get_node("../../Tutorializer").hide();

func tutorializer_previous():
	if(tutorializer_page > 0):
		tutorializer_page -= 1;
		get_node("../../Tutorializer/Tutorial-Text").text = tutorializer_text[tutorializer_page];
