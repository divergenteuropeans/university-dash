extends Node
onready var number = preload("res://puzzles/2048/number.tscn")
var counter = 0
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func spawn_number():
	var numberPos = Vector2()
	numberPos.x = rand_range(0, 200)
	numberPos.y = rand_range(0, 200)
	counter=counter+1
	if counter%2>0:
		add_child(number)
	else:
		add_child(number)
	number.set_position(numberPos)
	pass
	
	
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	spawn_number()
	spawn_number()
	spawn_number()
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
