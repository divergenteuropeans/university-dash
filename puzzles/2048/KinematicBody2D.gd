extends KinematicBody2D

# class member variables go here
var motion = Vector2()
var object_num = 0
var number = 2
var changedThisTurn = false
const UP = Vector2(0,-1)
const SPEED = 2000
func _ready(value):
	# Called every time the node is added to the scene.
	# Initialization here
	object_num = value
	pass

func _getNumber():
	return number

func _setNumber(input):
	number = input
	pass

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	if Input.is_action_pressed("ui_up"):
			motion.y = -SPEED
	elif Input.is_action_pressed("ui_down"):
			motion.y = SPEED
	else:
		motion.y = 0
	if Input.is_action_pressed("ui_right"):
		motion.x = SPEED	
	elif Input.is_action_pressed("ui_left"):
		motion.x = -SPEED
	else:
		motion.x = 0
	move_and_slide(motion)
	changedThisTurn = false
	pass


func _on_left_body_entered(body):
	_checkValue(body)
	pass

func _on_right_body_entered(body):
	_checkValue(body)
	pass # replace with function body


func _on_down_body_entered(body):
	_checkValue(body)
	pass # replace with function body


func _on_up_body_entered(body):
	_checkValue(body)
	pass # replace with function body

func _checkValue(body):
	if(get_class() != body.get_class()):
#		print(body.get_name()," no check")
#		print(number)
		pass
	elif(body.number == number && body.get_name() != get_name()):
#		print(body.get_name())
#		print(get_name())
		if(body.object_num < object_num):
			number=number*2
			body.queue_free()
		else:
			body.number=number*2
			queue_free()
		changedThisTurn = true
#		print(number)
	pass 
