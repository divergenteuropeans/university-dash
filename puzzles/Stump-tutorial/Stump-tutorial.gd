extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

# Array of all the tile nodes themselves. These are labels.
var tiles;
# Array of all the letters the buttons will use.
var letters;
# Node for the label containing all the words that have been used.
var answerspace;
# Node for the field for entering more words.
var entryfield;
# a list of all known words out of the dictionary file.
var words;
# List of all the words the user has successfully used
var used_words;
# Current score (int)
var score;
# Timer for the game. When it runs out, the game is done.
var timer;

# get_tiles()
#
# Populates an array of all the tiles and letters.
func get_tiles():
	tiles = [[
			get_node("Tile1/Label"),
			get_node("Tile2/Label"),
			get_node("Tile3/Label"),
			get_node("Tile4/Label")],
			[
			get_node("Tile5/Label"),
			get_node("Tile6/Label"),
			get_node("Tile7/Label"),
			get_node("Tile8/Label")],
			[
			get_node("Tile9/Label"),
			get_node("Tile10/Label"),
			get_node("Tile11/Label"),
			get_node("Tile12/Label")],
			[
			get_node("Tile13/Label"),
			get_node("Tile14/Label"),
			get_node("Tile15/Label"),
			get_node("Tile16/Label")]];
	letters = [['A','A','A','A'],['A','A','A','A'],['A','A','A','A'],['A','A','A','A']];

# _ready()
#
# Initializes the variables, then starts calling the init functions above.
# Then gets all the words out of the dictionary, and creates a random set of letters
# which it puts on the tiles in a random order.
# Starts the timer when all this is done.
func _ready():
	timer = get_node("Timer");
	score = 0;
	randomize();
	get_tiles();
	var words_file = File.new();
	words_file.open("res://puzzles/Stump/words", File.READ);
	words = [];
	used_words = [];
	while words_file.eof_reached() == false:
		words.append(words_file.get_line());
	answerspace = get_node("ScrollContainer/Words");
	entryfield = get_node("EntryField");
	var asciiletter;
	for row in range(0,4):
		for column in range(0,4):
			asciiletter = randi() % 25 + 65;
			letters[row][column] = char(asciiletter);
			tiles[row][column].text = letters[row][column];
	tutorializer_next();

# _process(delta)
#
# Just updates the timer every frame. Could be done in a less inefficient way,
# but we're already doing so much frame-by-frame adding this to the list is
# insignificant in the performance dimension.
func _process(delta):
	get_node("Time").text = str(int(timer.time_left));

# _on_EntryField_text_entered(new_text)
#
# Triggered when the user hits enter. Checks to see if the word is long enough,
# then checks it against the words dictionary, and records it. It calcuates score
# from there based on the length of the word. It turns out that word length is
# usually really hard to obtain, but unusual letters are not.
#
# When that is done, it gives the user feedback on what they enterd below the input
# field.
func _on_EntryField_text_entered(new_text):
	var isword = false;
	var usesletters = true;
	var usedtemp = false;
	var alreadyused = false;
	new_text = new_text.to_lower();
	if(new_text in words):
		isword = true;
	if(new_text.length() < 3):
		isword = false;
	new_text = new_text.to_upper();
	if(new_text in used_words):
		alreadyused = true;
	for letter in new_text:
		usedtemp = false;
		for row in letters:
			if letter in row:
				usedtemp = true;
		if usedtemp == false:
			usesletters = false;
	if isword and usesletters and !alreadyused:
		answerspace.text=new_text+"\n"+answerspace.text;
		used_words.append(new_text);
		match (new_text.length()):
			3:
				score += 5;
				get_node("Reasons").text = "Good.";
			4:
				score += 6;
				get_node("Reasons").text = "Great";
			5:
				score += 7;
				get_node("Reasons").text = "Nice";
			6,7,8,9:
				score += 8;
				get_node("Reasons").text = "Awesome!";
			10,11,12,13,14,15:
				score += 25;
				get_node("Reasons").text = "Amazing.";
			_:
				get_node("Reasons").text = "Incredible.";
				score += 100;
		get_node("Score").text = str(score)
	if !isword:
		get_node("Reasons").text = "I don't know that word.";
	if !usesletters:
		get_node("Reasons").text = "Be sure to use only the letters in the boxes to the right.";
	if alreadyused:
		get_node("Reasons").text = "That would work, but you've used it before!";
	if new_text.length() < 3:
		get_node("Reasons").text = "That's too short. Stick to three letters or more.";
	entryfield.text = "";
	pass # replace with function body

# _on_Timer_timeout()
#
# handles the end condition. Records the score and skill points and gives the
# user a prompt to return to campus.
func _on_Timer_timeout():
	get_node("EntryField").editable = false;
	get_node("EndSound").play();
	get_node("CampusReturn").show();

# backtocampus()
#
# Removes the scene and boots the player back into world.
func backtocampus():
	tutorializer_next();

var tutorializer_played = false;
var tutorializer_page = 0;
var tutorializer_text = [
	"Welcome to Stump!\n\nIn this game you'll see a grid of squares. In each square is a\nletter. At the bottom of the screen you'll see score, and a\nblank field for entering words. You'll want to click that field\nas soon as you start.",
	"The goal of the game is to form words out of the letters\nin the grid to the side. Forming a word will make it appear\nin a list above the entry field.\n\nYou want to type as long a word as you can, because\nthe longer the word the more points you get.\n\nYou're doing really well if you get above 100,\nthat's a good number to strive for!",
	"Be careful, you're on the clock here! You want to get as\nmany long words as you can in the time you have!\n\n\n\nWhen you're ready to give it a try, press 'Next'.",
	"That's it! You'll find Stump around the map when you start\n the game."];

func tutorializer_next():
	get_node("Tutorializer").show();
	if(tutorializer_page <= tutorializer_text.size()-1):
		if tutorializer_page == 3:
			if tutorializer_played == false:
				get_node("Tutorializer").hide();
				timer.start();
				tutorializer_played = true;
				return;
		get_node("Tutorializer/Tutorializer-Text").text = tutorializer_text[tutorializer_page];
		tutorializer_played = false;
		tutorializer_page += 1;
	else:
		get_node("Tutorializer").hide();
		self.queue_free();

func tutorializer_previous():
	if(tutorializer_page > 1):
		get_node("Tutorializer/Tutorializer-Text").text = tutorializer_text[tutorializer_page-2];
		tutorializer_page -= 1
