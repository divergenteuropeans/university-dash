extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

# Array of all the tile nodes themselves. These are labels.
var tiles;
# Array of all the letters the buttons will use.
var letters;
# Node for the label containing all the words that have been used.
var answerspace;
# Node for the field for entering more words.
var entryfield;
# a list of all known words out of the dictionary file.
var words;
# List of all the words the user has successfully used
var used_words;
# Current score (int)
var score;
# Timer for the game. When it runs out, the game is done.
var timer;

func set_letter(row, column):
	entryfield.text += letters[row][column];

func _input(event):
	if(Input.is_action_pressed("control_backspace")):
		entryfield.text = entryfield.text.substr(0, entryfield.text.length()-1);
	if(Input.is_action_pressed("ui_select")):
		_on_EntryField_text_entered(entryfield.text);

# get_tiles()
#
# Populates an array of all the tiles and letters.
func get_tiles():
	get_node("Tile1/TextureButton").grab_focus();
	var tile_buttons = [[
		get_node("Tile1/TextureButton"),
		get_node("Tile2/TextureButton"),
		get_node("Tile3/TextureButton"),
		get_node("Tile4/TextureButton")],
		[
		get_node("Tile5/TextureButton"),
		get_node("Tile6/TextureButton"),
		get_node("Tile7/TextureButton"),
		get_node("Tile8/TextureButton")],
		[
		get_node("Tile9/TextureButton"),
		get_node("Tile10/TextureButton"),
		get_node("Tile11/TextureButton"),
		get_node("Tile12/TextureButton")],
		[
		get_node("Tile13/TextureButton"),
		get_node("Tile14/TextureButton"),
		get_node("Tile15/TextureButton"),
		get_node("Tile16/TextureButton")]];
	for row_index in range(0, 4):
		for column_index in range(0,4):
			tile_buttons[row_index][column_index].connect("pressed", self, "set_letter", [row_index, column_index]);
			if column_index != 0:
				tile_buttons[row_index][column_index].focus_neighbour_left = tile_buttons[row_index][column_index - 1].get_path();
				tile_buttons[row_index][column_index - 1].focus_neighbour_right = tile_buttons[row_index][column_index].get_path();
			if row_index != 0:
				tile_buttons[row_index][column_index].focus_neighbour_top = tile_buttons[row_index - 1][column_index].get_path();
				tile_buttons[row_index - 1][column_index].focus_neighbour_bottom = tile_buttons[row_index][column_index].get_path();
	tiles = [[
			get_node("Tile1/Label"),
			get_node("Tile2/Label"),
			get_node("Tile3/Label"),
			get_node("Tile4/Label")],
			[
			get_node("Tile5/Label"),
			get_node("Tile6/Label"),
			get_node("Tile7/Label"),
			get_node("Tile8/Label")],
			[
			get_node("Tile9/Label"),
			get_node("Tile10/Label"),
			get_node("Tile11/Label"),
			get_node("Tile12/Label")],
			[
			get_node("Tile13/Label"),
			get_node("Tile14/Label"),
			get_node("Tile15/Label"),
			get_node("Tile16/Label")]];
	letters = [['A','A','A','A'],['A','A','A','A'],['A','A','A','A'],['A','A','A','A']];

func gen_letters():
	var asciiletter;
	var gotvowel = false;
	for row in range(0,4):
		for column in range(0,4):
			asciiletter = randi() % 25 + 65;
			letters[row][column] = char(asciiletter);
			tiles[row][column].text = letters[row][column];
	for row in range(0,4):
		for column in range(0,4):
			if letters[row][column] == 'A' || letters[row][column] == 'E' || letters[row][column] == 'I' || letters[row][column] == 'O' || letters[row][column] == 'U':
				gotvowel = true;
	if gotvowel == false:
		print("fiasco 2.0 aversion code....\n\nINITIATED!")
		gen_letters();

# _ready()
#
# Initializes the variables, then starts calling the init functions above.
# Then gets all the words out of the dictionary, and creates a random set of letters
# which it puts on the tiles in a random order.
# Starts the timer when all this is done.
func _ready():
	timer = get_node("Timer");
	score = 0;
	randomize();
	get_tiles();
	var words_file = File.new();
	words_file.open("res://puzzles/Stump/words", File.READ);
	words = [];
	used_words = [];
	while words_file.eof_reached() == false:
		words.append(words_file.get_line());
	answerspace = get_node("ScrollContainer/Words");
	entryfield = get_node("EntryField");
	gen_letters();
	timer.start();

# _process(delta)
#
# Just updates the timer every frame. Could be done in a less inefficient way,
# but we're already doing so much frame-by-frame adding this to the list is
# insignificant in the performance dimension.
func _process(delta):
	get_node("Time").text = str(int(timer.time_left));

# _on_EntryField_text_entered(new_text)
#
# Triggered when the user hits enter. Checks to see if the word is long enough,
# then checks it against the words dictionary, and records it. It calcuates score
# from there based on the length of the word. It turns out that word length is
# usually really hard to obtain, but unusual letters are not.
#
# When that is done, it gives the user feedback on what they enterd below the input
# field.
func _on_EntryField_text_entered(new_text):
	var isword = false;
	var usesletters = true;
	var usedtemp = false;
	var alreadyused = false;
	new_text = new_text.to_lower();
	if(new_text in words):
		isword = true;
	if(new_text.length() < 3):
		isword = false;
	new_text = new_text.to_upper();
	if(new_text in used_words):
		alreadyused = true;
	for letter in new_text:
		usedtemp = false;
		for row in letters:
			if letter in row:
				usedtemp = true;
		if usedtemp == false:
			usesletters = false;
	if isword and usesletters and !alreadyused:
		answerspace.text=new_text+"\n"+answerspace.text;
		used_words.append(new_text);
		match (new_text.length()):
			3:
				score += 5;
				get_node("Reasons").text = "Good.";
			4:
				score += 6;
				get_node("Reasons").text = "Great";
			5:
				score += 7;
				get_node("Reasons").text = "Nice";
			6,7,8,9:
				score += 8;
				get_node("Reasons").text = "Awesome!";
			10,11,12,13,14,15:
				score += 25;
				get_node("Reasons").text = "Amazing.";
			_:
				get_node("Reasons").text = "Incredible.";
				score += 100;
		get_node("Score").text = str(score)
	if !isword:
		get_node("Reasons").text = "I don't know that word.";
	if !usesletters:
		get_node("Reasons").text = "Be sure to use only the letters in the boxes to the right.";
	if alreadyused:
		get_node("Reasons").text = "That would work, but you've used it before!";
	if new_text.length() < 3:
		get_node("Reasons").text = "That's too short. Stick to three letters or more.";
	entryfield.text = "";
	pass # replace with function body

# _on_Timer_timeout()
#
# handles the end condition. Records the score and skill points and gives the
# user a prompt to return to campus.
func _on_Timer_timeout():
	get_node("EntryField").editable = false;
	get_node("EndSound").play();
	get_node("CampusReturn").show();
	var totalskills = int(score / 15);
	var chance = 0;
	if(totalskills != 0):
		chance = randi() % totalskills;
	gamestate.get_ClientPlayer().updateStats({
		creativity = 0,
		logic = 0,
		communication = 0,
		initiative = chance,
		humanity = totalskills - chance,
		vocational = 0,
		unassigned = 0})

# backtocampus()
#
# Removes the scene and boots the player back into world.
func backtocampus():
	var tree_root = get_node("/root/world");
	tree_root.remove_child(tree_root.get_child(tree_root.get_child_count() -1));
