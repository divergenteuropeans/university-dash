extends AnimationPlayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var currentnode;

# Named in such a way to suggest a boolean, actually an integer used to track
# position in the song.
var isplaying;

# These two are used simply to track which notes have a selection. This means
# that programming a song can be just setting tiles in the Notes map.
var notesnode;
var usednotes;

# The current 1/16th of a note the user is on. This doesn't start at zero when
# the song starts, but instead tracks the song as it plays so notes can be
# checked for validity, etc.
var currentpos;

# Score variables. It turns out rouge input after the song has ended can affect
# the player's score (such as clicking the mouse, oddly enough) and so it makes
# sense to have a final score variable that is set on puzzle completion.
var currentscore;
var finalscore;

# A couple of nodes. Readysetgo is where the score goes, cheers are for
# compliments that we shower on our users to make their progress feel good.
var readySetGo;
var cheers;

# Booleans for whether these keys have been presssed during the current
# sixteenth. This is so key bounce or repeat can't grant them score more than
# once for hitting the right note during the current sixteenth.
var fkey;
var gkey;
var hkey;
var jkey;

# Animation player nodes. Used to start animations when the user presses a key.
var fanimation;
var ganimation;
var hanimation;
var janimation;

# _ready()
#
# processes some intialization tasks. Grabs variables from various places based
# on static constants or functions to other objects or just get_node()'s
func _ready():
	set_process_unhandled_input(true);
	currentnode  = get_node("ScrollingMap");
	notesnode    = get_node("ScrollingMap/Notes")
	usednotes    = notesnode.get_used_cells();
	isplaying    = 16;
	currentscore = 0;
	currentpos   = 31;
	readySetGo   = get_node("ReadySetGo");
	cheers       = get_node("Cheers");
	fkey         = false;
	gkey         = false;
	hkey         = false;
	jkey         = false;
	fanimation   = get_node("../../FAnimation");
	ganimation   = get_node("../../GAnimation");
	hanimation   = get_node("../../HAnimation");
	janimation   = get_node("../../JAnimation");
	startscroll()

# _input(event)
#
# handles keyboard input for notes. This means that the keyboard input for
# fghj can be processed correctly for each frame and if a user pushes a key
# partway through a pause in animation it still works.
func _input(event):
	var got = false;
	var key = false;
	# The goal with each of these letter handlers is to check whether we have
	# a note under the currently pressed letter, and to then dole out points
	# based on how close we are.
	if(Input.is_action_pressed("Rhythm_F")):
		key = true;
		if(fkey == false):
			fkey = true;
			if(Vector2(4, currentpos-1) in usednotes):
				currentscore+=1
				got = true;
				if(!fanimation.is_playing()):
					fanimation.play("BlowUp");
			elif(Vector2(4, currentpos) in usednotes):
				currentscore+=5;
				got = true;
				if(!fanimation.is_playing()):
					fanimation.play("BlowUp-Green");
			elif(Vector2(4, currentpos+1) in usednotes):
				currentscore+=1
				got = true;
				if(!fanimation.is_playing()):
					fanimation.play("BlowUp");
			else:
				if(!fanimation.is_playing()):
					fanimation.play("BlowUp-Grey");
	if(Input.is_action_pressed("Rhythm_G")):
		key = true;
		if(Vector2(5, currentpos-1) in usednotes):
			if(gkey == false):
				currentscore+=1
				if(!ganimation.is_playing()):
					ganimation.play("BlowUp");
			got = true;
		elif(Vector2(5, currentpos) in usednotes):
			if(gkey == false):
				currentscore+=5;
				if(!ganimation.is_playing()):
					ganimation.play("BlowUp-Green");
			got = true;
		elif(Vector2(5, currentpos+1) in usednotes):
			if(gkey == false):
				currentscore+=1
				if(!ganimation.is_playing()):
					ganimation.play("BlowUp");
			got = true;
		else:
			if(gkey == false):
				if(!ganimation.is_playing()):
					ganimation.play("BlowUp-Grey");
	if(Input.is_action_pressed("Rhythm_H")):
		key = true;
		if(hkey == false):
			hkey = true;
			if(Vector2(6, currentpos-1) in usednotes):
				currentscore+=1
				if(!hanimation.is_playing()):
					hanimation.play("BlowUp");
				got = true;
			elif(Vector2(6, currentpos) in usednotes):
				currentscore+=5;
				if(!hanimation.is_playing()):
					hanimation.play("BlowUp-Green");
				got = true;
			elif(Vector2(6, currentpos+1) in usednotes):
				currentscore+=1
				if(!hanimation.is_playing()):
					hanimation.play("BlowUp");
				got = true;
			else:
				if(!hanimation.is_playing()):
					hanimation.play("BlowUp-Grey");
	if(Input.is_action_pressed("Rhythm_J")):
		key = true;
		if(jkey == false):
			jkey = true;
			if(Vector2(7, currentpos-1) in usednotes):
				currentscore+=1
				if(!janimation.is_playing()):
					janimation.play("BlowUp");
				got = true;
			elif(Vector2(7, currentpos) in usednotes):
				currentscore+=5;
				if(!janimation.is_playing()):
					janimation.play("BlowUp-Green");
				got = true;
			elif(Vector2(7, currentpos+1) in usednotes):
				currentscore+=1
				if(!janimation.is_playing()):
					janimation.play("BlowUp");
				got = true;
			else:
				if(!janimation.is_playing()):
					janimation.play("BlowUp-Grey");
	if(!got && key):
		currentscore -= 3;
	fkey = "F" in event.as_text();
	gkey = "G" in event.as_text();
	hkey = "H" in event.as_text();
	jkey = "J" in event.as_text();

# startscroll()
#
# starts the page moving.
func startscroll():
	get_node("../AnimationPlayer2").play("Drift");
	get_node(".").play("New Anim");

# scrollpage()
#
# moves the page an interval as triggered by the animationplayer.
# this keeps the notes in synch with the beats of the music constantly and
# requires no timer.
func scrollpage():
	if(isplaying==0):
		get_node("AudioStreamPlayer").play();
		get_node("ScoreLabel").show();
	if(isplaying==8):
		readySetGo.text = "Set!";
	if(isplaying==4):
		readySetGo.text = "Go!";
	if(isplaying <= 0):
		readySetGo.text = str(currentscore);
		if(currentscore < 20):
			cheers.text = "Let's Go!"
		elif(currentscore < 50):
			cheers.text = "Good Start!";
		elif(currentscore < 200):
			cheers.text = "Nice!";
		elif(currentscore < 800):
			cheers.text = "Great!";
		elif(currentscore < 1000):
			cheers.text = "Awesome!";
		elif(currentscore < 1500):
			cheers.text = "Simply Amazing!";
		elif(currentscore > 1500):
			cheers.text = "LEGENDARY.";
	isplaying  -=1;
	currentpos -=1;
	fkey = false;
	gkey = false;
	hkey = false;
	jkey = false;
	currentnode.global_translate(Vector2(0,48))
	if(Vector2(-1, currentpos-1) in usednotes):
		get_node(".").stop(false);
		get_node("../AnimationPlayer2").stop(false);
		get_node("Backbutton").show();
		finalscore = currentscore;
		var totalskills = int(finalscore / 110);
		var chance;
		if(totalskills > 0):
			chance = randi() % totalskills + 1;
		else:
			chance = 0
		gamestate.get_ClientPlayer().updateStats({
			creativity    = chance,
			logic         = 0,
			communication = totalskills - chance,
			initiative    = 0,
			humanity      = 0,
			vocational    = 0,
			unassigned    = 0})

# _on_Backbutton_pressed()
#
# Handles when the user tries to return to campus. They should be able to.
func _on_Backbutton_pressed():
	var tree_root = get_node("/root/world");
	tree_root.remove_child(tree_root.get_child(tree_root.get_child_count() -1));
