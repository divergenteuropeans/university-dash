extends AnimationPlayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var currentnode;

# Named in such a way to suggest a boolean, actually an integer used to track
# position in the song.
var isplaying;

# These two are used simply to track which notes have a selection. This means
# that programming a song can be just setting tiles in the Notes map.
var notesnode;
var usednotes;

var currentpos;

var currentscore;
var readySetGo;

func _ready():
	set_process_unhandled_input(true);
	# Initialization here
	currentnode  = get_node("ScrollingMap");
	notesnode    = get_node("ScrollingMap/Notes")
	usednotes    = notesnode.get_used_cells();
	isplaying    = 16;
	currentscore = 0;
	currentpos   = 31;
	readySetGo   = get_node("ReadySetGo");
	startscroll()
	print("running script OK");

func _input(event):
	if(event.as_text() == "F"):
		if(Vector2(4, currentpos+1) in usednotes):
			currentscore+=5;
	if(event.as_text() == "G"):
		if(Vector2(5, currentpos+1) in usednotes):
			currentscore+=5;
	if(event.as_text() == "H"):
		if(Vector2(6, currentpos+1) in usednotes):
			currentscore+=5;
	if(event.as_text() == "J"):
		if(Vector2(7, currentpos+1) in usednotes):
			currentscore+=5;
#	if(event.type == InputEvent.KEY):
#		if(event.scancode == KEY_J):
#			if(Vector2(4, currentpos) in usednotes):
#				currentscore+=5;
#			pass #eventually needs to trigger event checking cell for first note.
	pass;

func startscroll():
	get_node("../AnimationPlayer2").play("Drift");
	get_node(".").play("New Anim");

func scrollpage():
	if(isplaying==0):
		get_node("AudioStreamPlayer").play();
		get_node("ScoreLabel").show();
	if(isplaying==8):
		readySetGo.text = "Set!";
	if(isplaying==4):
		readySetGo.text = "Go!";
	if(isplaying <= 0):
		readySetGo.text = str(currentscore);
	isplaying  -=1;
	currentpos -=1;
	currentnode.global_translate(Vector2(0,48))

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
