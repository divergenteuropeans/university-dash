#include<iostream>
using namespace std;

#ifndef BOARDSIZE
#define BOARDSIZE 9
#define HORIZ 3
#define VERTICAL 3
#endif

void display_board()
{
  cout<<endl;
  for(
    int row_index = 0;
    row_index < BOARDSIZE;
    row_index ++)
  {
    if(
      row_index != 0 &&
      row_index % HORIZ == 0)
    {
      cout<<endl;
    }
    for(
      int column_index = 0;
      column_index < BOARDSIZE;
      column_index ++)
    {
      if(
        column_index == 0 ||
        column_index % VERTICAL == 0)
      {
        cout<<"  ";
      }
      cout<<board[row_index][column_index].value
          <<" ";
    }
    cout<<endl;
  }
  cout<<endl;
}
