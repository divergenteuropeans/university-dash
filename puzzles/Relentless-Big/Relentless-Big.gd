extends CanvasLayer

# Size of the relentless board
var HORIZSIZE = 9;
var VERTSIZE = 7;

# Score starts low, then rises as the user actually plays, encouraging
# the player to actually play. It starts decreasing after a certain number
# of clicks.
var score = 895;
var clicks = 0;

# List of tiles, and list of visited tiles so we can make them green.
var tiles;
var visitedtiles;

# List of hazards and tracking variable for the number of hazards around
# the currently selected tile.
var hazards;
var numsurrounding=0;

# Some textures we need to be able to assign a lot. These are globals to allow
# for any function to modify the tile as needed.
var knocked_texture;
var cleared_texture;
var current_texture;
var potential_texture;
var cleardisabled_texture;
var disabled_texture;
var selected_texture;

# makehazards()
#
# Generates the hazards array, then actually populates it with things for the user
# to run into.
func makehazards():
	hazards = [[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}],[
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false},
	{ishazard=false, visited=false}]];
	var potential;
	var lasttwo = 0;
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			potential = randi() %7;
			if(potential < 2 && lasttwo < 2 && (row_index > 1 || column_index >1) && (row_index < VERTSIZE-2 || column_index < HORIZSIZE-2)):
				hazards[row_index][column_index].ishazard = true;
				#tiles[row_index][column_index].texture_disabled = knocked_texture;
				lasttwo += 1;
			else:
				lasttwo -= 1;

# gettiles()
#
# gets all the tile nodes in the map. This allows us to modify any tile we
# like at any point in time. It also resets the visited tiles.
func gettiles():
	visitedtiles=[];
	tiles = [[
		get_node("Tiles/Tile1"),
		get_node("Tiles/Tile2"),
		get_node("Tiles/Tile3"),
		get_node("Tiles/Tile4"),
		get_node("Tiles/Tile5"),
		get_node("Tiles/Tile6"),
		get_node("Tiles/Tile7"),
		get_node("Tiles/Tile8"),
		get_node("Tiles/Tile9")],
		[
		get_node("Tiles/Tile10"),
		get_node("Tiles/Tile11"),
		get_node("Tiles/Tile12"),
		get_node("Tiles/Tile13"),
		get_node("Tiles/Tile14"),
		get_node("Tiles/Tile15"),
		get_node("Tiles/Tile16"),
		get_node("Tiles/Tile17"),
		get_node("Tiles/Tile18")],
		[
		get_node("Tiles/Tile19"),
		get_node("Tiles/Tile20"),
		get_node("Tiles/Tile21"),
		get_node("Tiles/Tile22"),
		get_node("Tiles/Tile23"),
		get_node("Tiles/Tile24"),
		get_node("Tiles/Tile25"),
		get_node("Tiles/Tile26"),
		get_node("Tiles/Tile27")],
		[
		get_node("Tiles/Tile28"),
		get_node("Tiles/Tile29"),
		get_node("Tiles/Tile30"),
		get_node("Tiles/Tile31"),
		get_node("Tiles/Tile32"),
		get_node("Tiles/Tile33"),
		get_node("Tiles/Tile34"),
		get_node("Tiles/Tile35"),
		get_node("Tiles/Tile36")],
		[
		get_node("Tiles/Tile37"),
		get_node("Tiles/Tile38"),
		get_node("Tiles/Tile39"),
		get_node("Tiles/Tile40"),
		get_node("Tiles/Tile41"),
		get_node("Tiles/Tile42"),
		get_node("Tiles/Tile43"),
		get_node("Tiles/Tile44"),
		get_node("Tiles/Tile45")],
		[
		get_node("Tiles/Tile46"),
		get_node("Tiles/Tile47"),
		get_node("Tiles/Tile48"),
		get_node("Tiles/Tile49"),
		get_node("Tiles/Tile50"),
		get_node("Tiles/Tile51"),
		get_node("Tiles/Tile52"),
		get_node("Tiles/Tile53"),
		get_node("Tiles/Tile54")],
		[
		get_node("Tiles/Tile55"),
		get_node("Tiles/Tile56"),
		get_node("Tiles/Tile57"),
		get_node("Tiles/Tile58"),
		get_node("Tiles/Tile59"),
		get_node("Tiles/Tile60"),
		get_node("Tiles/Tile61"),
		get_node("Tiles/Tile62"),
		get_node("Tiles/Tile63")
		]];

# disableall()
#
# Disables all the tiles for instances where the user has run into something
# so they can't win the game with the you loose screen up.
func disableall():
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			tiles[row_index][column_index].disabled=true;

# textureresetall()
#
# Resets everything, so visited tiles don't show up from the last try of the
# game.
func textureresetall():
	visitedtiles.clear();
	tiles[0][0].grab_focus();
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			tiles[row_index][column_index].texture_disabled = disabled_texture;
			tiles[row_index][column_index].texture_normal = potential_texture;
			tiles[row_index][column_index].texture_focused = selected_texture;

# clickedtile()
#
# handles when a tile is clicked. This does everything from checking tiles around the
# clicked tiles to activating and deactivating tiles so that only the surrounding ones
# are clickable, to awarding/subtracting points based on the number of clicks.
#
# it also handles the win condition of reaching the opposite corner.
func clickedtile(rowindex, columnindex):
	get_node("Selected").play()
	visitedtiles.append([rowindex,columnindex]);
	clicks += 1
	if clicks < 21:
		score += 5;
	else:
		score -= 3;
	numsurrounding = 0;
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			if ((row_index >= rowindex - 1 && row_index <= rowindex + 1) && (column_index >= columnindex - 1 && column_index <= columnindex + 1)):
				tiles[row_index][column_index].disabled=false;
				if(hazards[row_index][column_index].ishazard == true):
					numsurrounding+=1
			else:
				tiles[row_index][column_index].disabled=true;
			if([row_index,column_index] in visitedtiles):
				tiles[row_index][column_index].texture_normal = cleared_texture;
				tiles[row_index][column_index].texture_disabled = cleared_texture;
	if(hazards[rowindex][columnindex].ishazard == true):
		tiles[rowindex][columnindex].texture_normal = knocked_texture;
		tiles[rowindex][columnindex].texture_disabled = knocked_texture;
		disableall();
		get_node("Tryagain").show();
		get_node("Tryagain").grab_focus();
		get_node("Fail").play();
		score -= 10;
	else:
		tiles[rowindex][columnindex].texture_normal = current_texture;
	if(rowindex == VERTSIZE -1 && columnindex == HORIZSIZE - 1):
		disableall();
		get_node("Continue").show();
		get_node("Continue").grab_focus();
		get_node("Success").play();
		var totalskills = int(score / 100);
		var chance = randi() % totalskills;
		gamestate.get_ClientPlayer().updateStats({
			creativity = 0,
			logic = chance,
			communication = 0,
			initiative = totalskills - int(chance / 2),
			humanity = 0,
			vocational = totalskills - int(chance / 2),
			unassigned = 0})
	get_node("Obstacles").text = str(numsurrounding);
	get_node("Score").text = str(score);

# restart()
#
# function called on the try again button. It resets everything but the score
# and allows for multiple retries to factor into a person's final score in the
# game.
func restart():
	makehazards();
	textureresetall();
	clickedtile(0,0);
	get_node("Tryagain").hide();

# connectstuff()
#
# connects a bunch of signal handlers and calls it a day.
func connectstuff():
	for row_index in range(0, VERTSIZE):
		for column_index in range(0, HORIZSIZE):
			tiles[row_index][column_index].connect("pressed", self, "clickedtile", [row_index,column_index]);

# _ready()
#
# Inits some variables. It then calls the neccesary components to start the game.
func _ready():
	randomize();
	
	knocked_texture = ImageTexture.new();
	knocked_texture.load("res://puzzles/Relentless/knocked_tile.svg");
	
	cleared_texture = ImageTexture.new();
	cleared_texture.load("res://puzzles/Relentless/cleared_tile.svg");
	
	current_texture = ImageTexture.new();
	current_texture.load("res://puzzles/Relentless/current_tile.svg");
	
	potential_texture = ImageTexture.new();
	potential_texture.load("res://puzzles/Relentless/potential_tile.svg");
	
	cleardisabled_texture = ImageTexture.new();
	cleardisabled_texture.load("res://puzzles/Relentless/cleared_tile_disabled.svg");
	
	disabled_texture = ImageTexture.new();
	disabled_texture.load("res://puzzles/Relentless/unexplored_tile.svg");
	
	selected_texture = ImageTexture.new();
	selected_texture.load("res://puzzles/Relentless/selected_tile.svg");
	
	gettiles();
	restart();
	connectstuff();

# backtocampus()
#
# Boots the player back to campus by forcefully removing the map.
# The users feel a jolt of dejection as the scene excuses itself from
# their view.
#
# or something.
func backtocampus():
	var tree_root = get_node("/root/world");
	tree_root.remove_child(tree_root.get_child(tree_root.get_child_count() -1));
