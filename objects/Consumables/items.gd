extends Area2D

# >NO HEADERS???
onready var id = str (get_tree().get_network_unique_id())

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
var inrange = false
var justpressed = true
var itemType
func _input(event):
	if(Input.is_action_pressed("select_item") && inrange == true && justpressed == false):
		justpressed = true
		if(itemType == "simple"):
			gamestate.get_ClientPlayer().get_node("itemMenu").add_simple(1)
		if(itemType == "quality"):
			gamestate.get_ClientPlayer().get_node("itemMenu").add_quality(1)
		if(itemType == "energy"):
			gamestate.get_ClientPlayer().get_node("itemMenu").add_energy(1)
		queue_free()




func _on_simple_item_body_entered(body):
	if (body.get_name() == id) and gamestate.get_ClientPlayer().isPlayerGraduated == false:
		inrange = true
	itemType = "simple"
	justpressed = false
func _on_simple_item_body_exited(body):
	if (body.get_name() == id):
		inrange = false

func _on_quality_meal_body_entered(body):
	if (body.get_name() == id) and gamestate.get_ClientPlayer().isPlayerGraduated == false:
		inrange = true
	itemType = "quality"
	justpressed = false
func _on_quality_meal_body_exited(body):
	if (body.get_name() == id):
		inrange = false

func _on_energy_drink_body_entered(body):
	if (body.get_name() == id) and gamestate.get_ClientPlayer().isPlayerGraduated == false:
		inrange = true
	itemType = "energy"
	justpressed = false
func _on_energy_drink_body_exited(body):
	if (body.get_name() == id):
		inrange = false