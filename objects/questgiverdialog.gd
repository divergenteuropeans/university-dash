extends CanvasLayer

# Object Primatives
var _background
var _portrait
var _message
# Member Vars
var _yes
var _no
var points


func _ready():
	_background = get_node("MarginContainer/bg")
	_portrait 	= get_node("MarginContainer/portrait")
	_message	= get_node("Panel/HBoxContainer/RichTextLabel")
	gamestate.get_ClientPlayer().hud_hide()


func set_diag(port, text, points):
#	_background.set_texture(bg)
	_portrait.set_texture(load(port))
	_message.set_text(text)
	self.points = points
	
#	func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_Yes_pressed():
	if self.points >= 0:
		var diag = 	load("res://puzzles/points_allocation.tscn")
		diag = diag.instance()
		get_tree().get_root().get_node("world").add_child(diag)
		diag.updatePool(points)
		self.queue_free()
		gamestate.get_ClientPlayer().hud_show()
	else: #To re-use old script files and to save time, the graduate screen is created from a variation of the quest dialog.
			#It is activated by sending a -1 value for the skill points allocation.
		var diag = 	load("res://puzzles/graduate_screen.tscn")
		diag = diag.instance()
		get_tree().get_root().get_node("world").add_child(diag)
		diag.setPool(0)
		self.queue_free()
		gamestate.get_ClientPlayer().hud_show()

func _on_No_pressed():
	self.queue_free()
	gamestate.get_ClientPlayer().hud_show()