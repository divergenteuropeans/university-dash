extends StaticBody2D

# Foreign
var players
var id
var player
# Object Primatives
var _shape	#Collision Shape
var _radius	# To detect Player
# Membar Vars
var inRange = false

func _ready():
	players = get_node("/root/world/players")		#Get Players Collection
	id = str (get_tree().get_network_unique_id())	# Get User ID
	player = players.get_node(id)
	
	_shape  = get_node("CollisionShape2D")
	_radius = get_node("Radius")


func _process(delta):
	if (Input.is_action_pressed("set_bomb") and inRange == true):
		loadEvent()


func loadEvent():
	var map = 	load("res://puzzles/rhythm_veeex.tscn")
	map = map.instance()
	get_tree().get_root().get_node("world").add_child(map)


func _on_Area2D_body_entered(body):
	if (body.get_name() == id):
		inRange = true

func _on_Area2D_body_exited(body):
	if (body.get_name() == id):
		inRange = false