extends StaticBody2D

# Foreign
onready var id = str (get_tree().get_network_unique_id())
var player
var playerStats
# Object Primatives
var _name
var _animate
var _textBox
var _sprite
# Member Vars
var inRange = false
var reqMet = false

### NPC Parameters ###
### Requirements:	creativity, logic, communication, 
###				initiative, humanity , vocational
onready var requirementName = "creativity"
onready var requirementVal = 10
var portrait = "res://objects/NPCPortraits/Mary.png"
var points = 7
var dialog = "Hey, stranger. You're the new kid in my class aren't you?\nWant to work on a project together? \n\n (Reward: " + str(points) + " Skill Points)"


func _ready():
	_name = get_node("label")
	_name.set_text("Mary")
	_animate = get_node("RequirementsBox/animate")
	_textBox = get_node("RequirementsBox/DialogBox/Text")
	_textBox.set_text(" Requires: "+String(requirementVal)+" "+requirementName+" ")
#	_sprite.set_texture("")

func _process(delta):
	if (Input.is_action_pressed("select_item") and inRange == true and reqMet == true):
		loadEvent()

func loadEvent():
	var map = 	load("res://objects/questgiverdialog.tscn")
	map = map.instance()
	get_tree().get_root().get_node("world").add_child(map)
	map.set_diag(portrait, dialog, points)
	self.queue_free()
#	var map = 	load("res://objects/questgiverdialog.tscn")
#	get_tree().change_scene("res://objects/questgiverdialog.tscn")


func _on_ActivateTrigger_body_entered(body):
	player = gamestate.get_ClientPlayer()
	playerStats = player.getStats()
	
	if playerStats[requirementName] >= requirementVal:
		reqMet = true
	if (body.get_name() == id):
		inRange = true

func _on_ActivateTrigger_body_exited(body):
	if (body.get_name() == id):
		inRange = false


## Chat Bubble
func _on_ChatTrigger_body_entered(body):
	if (body.get_name() == id):
		_animate.play("fade_in")
func _on_ChatTrigger_body_exited(body):
	if (body.get_name() == id):
		_animate.play_backwards("fade_in")