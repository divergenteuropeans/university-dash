extends StaticBody2D

# Foreign
onready var id = str (get_tree().get_network_unique_id())
var player
var playerStats
# Object Primatives
var _name
var _animate
var _textBox
# Member Vars
var inRange = false
var reqMet = false

#Requirements:	creativity, logic, communication, 
#				initiative, humanity , vocational
onready var requirementName = "logic"
onready var requirementName2 = "initiative"
onready var requirementVal = 15
onready var requirementVal2 = 3
var portrait = "res://objects/NPCPortraits/Agent.png"
var points = 12
var dialog = "Hey, you! Stop right right there!\nHeh, you're not under arrest... yet. The United States could use your talent though... \n\n (Reward: " + str(points) + " Skill Points)"


func _ready():
	_name = get_node("label")
	_name.set_text("Agent")
	_animate = get_node("RequirementsBox/animate")
	_textBox = get_node("RequirementsBox/DialogBox/Text")
	_textBox.set_text(" Requires: "+String(requirementVal)+" "+requirementName+" "+String(requirementVal2)+String(requirementVal2)+" "+requirementName2+"")

func _process(delta):
	if (Input.is_action_pressed("select_item") and inRange == true and reqMet == true):
		loadEvent()

func loadEvent():
	var map = 	load("res://objects/questgiverdialog.tscn")
	map = map.instance()
	get_tree().get_root().get_node("world").add_child(map)
	map.set_diag(portrait, dialog, points)
	self.queue_free()
#	var map = 	load("res://objects/questgiverdialog.tscn")
#	get_tree().change_scene("res://objects/questgiverdialog.tscn")


func _on_ActivateTrigger_body_entered(body):
	player = gamestate.get_ClientPlayer()
	playerStats = player.getStats()
	if playerStats[requirementName] >= requirementVal and playerStats[requirementName2] >= requirementVal2:
		reqMet = true
	if (body.get_name() == id):
		inRange = true

func _on_ActivateTrigger_body_exited(body):
	if (body.get_name() == id):
		inRange = false


## Chat Bubble
func _on_ChatTrigger_body_entered(body):
	if (body.get_name() == id):
		_animate.play("fade_in")
func _on_ChatTrigger_body_exited(body):
	if (body.get_name() == id):
		_animate.play_backwards("fade_in")