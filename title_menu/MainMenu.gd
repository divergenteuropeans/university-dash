extends Control

# Object Primatives
var _noteTooltip
var _tooltipLabel
var _animate
# Misc 
var isShown = false
var timer
var time 	= 2

var tree_root;
var current_scene;

func _ready():
	# In order to do custom loading of subscenes (keep the music playing)
	# We'll need the tree root a few times.
	tree_root = get_tree().get_root();
	current_scene = tree_root.get_child(tree_root.get_child_count() -1);
	_noteTooltip 	= get_node("NoteToolTip")
	_tooltipLabel 	= get_node("NoteToolTip/ToolTipLabel")
	_animate 		= get_node("Animate")
	# Hide timer
	timer = Timer.new()
	timer.set_wait_time(time)
	timer.connect("timeout", self, "HideNote")
	add_child(timer)	#Add to Scene
	get_node("VBoxContainer/JoinButton").grab_focus();
	pass

## TOOLTIP LABELING ##
func _on_JoinButton_mouse_entered():
	_tooltipLabel.set_text("Start the game.\n\nYou'll need to\neither host or enter\n the IP Address of the\nperson hosting.")
	ShowNote()
func _on_JoinButton_mouse_exited():
	timer.start()

func _on_LearnButton_mouse_entered():
	_tooltipLabel.set_text("Learn the game.\n\nLearn how each\npuzzle works before\nstarting the game")
	ShowNote()
func _on_HostButton_mouse_exited():
	timer.start()

func _on_OptionsButton_mouse_entered():
	_tooltipLabel.set_text("Access the options.\n\nYou can change and\ncustomize the\nin-game settings.")
	ShowNote()
func _on_OptionsButton_mouse_exited():
	timer.start()

func _on_ExitButton_mouse_entered():
	_tooltipLabel.set_text("Exit the game.\n")
	ShowNote()
func _on_ExitButton_mouse_exited():
	timer.start()
	
## Animation Functions
func ShowNote():
	timer.stop()
	if !isShown:
		_noteTooltip.show()
		_animate.play("fade_in")
		isShown = !isShown

func HideNote():
	_animate.play_backwards("fade_in")
	isShown = !isShown
func _on_Animate_animation_finished(anim_name):
	if !isShown:
		_noteTooltip.hide()


## INPUT EVENTS ## 
func _on_pressed():
	get_node("Selected").play();

func _on_JoinButton_pressed():
	var subscene = ResourceLoader.load("res://title_menu/lobby.tscn");
	get_tree().get_root().add_child(subscene.instance());

func _on_ExitButton_pressed():
	get_tree().quit()

func _on_OptionsButton_pressed():
	var subscene = ResourceLoader.load("res://title_menu/options_screen.tscn");
	get_tree().get_root().add_child(subscene.instance());
	pass # replace with function body


func _on_LearnButton_pressed():
	var subscene = ResourceLoader.load("res://title_menu/TutorializerMenu.tscn");
	get_tree().get_root().add_child(subscene.instance());
	pass # replace with function body