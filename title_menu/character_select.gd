extends Label

# Object Primatives
var _animate
var _charsprite
# Script Vars
var path = []
var index = 0


func _ready():
	_animate = get_node("sprite/animate")
	_charsprite = get_node("sprite")
	populateList()
	load_spritesheet()
	pass

func load_spritesheet():
	var tex_path = path[index]
	_charsprite.set_texture(load(tex_path))
    
	#Reference Methods
	#_charsprite.set_hframes(1)
	#_charsprite.set_hframes(1)


func _on_Forward_pressed():
	if index < len(path)-1:
		index += 1
	else:
		index = 0
	load_spritesheet()
func _on_Backward_pressed():
	if index > 0:
		index -= 1
	else:
		index = len(path)-1
	load_spritesheet()


# Lobby-to-Gamestate Function 
func get_skin():
	return path[index]



func populateList():
	#Boy
	path.append("res://player/charwalks/boy/charwalk_boy_blondehair_blueshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_redhair_redshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_blondehair_purpleshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_blondehair_redshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_brownhair_blueshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_brownhair_greenshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_brownhair_purpleshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_brownhair_redshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_darkhair_blueshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_darkhair_greenshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_darkhair_purpleshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_darkhair_redshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_redhair_blueshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_redhair_greenshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_redhair_purpleshirt.png")
	path.append("res://player/charwalks/boy/charwalk_boy_redhair_redshirt.png")
	
	#Girl
	# TODO Set Seperate List
	path.append("res://player/charwalks/girl/charwalk_girl_blondehair_blueshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_blondehair_greenshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_blondehair_purpleshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_blondehair_redshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_brownhair_blueshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_brownhair_greenshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_brownhair_purpleshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_brownhair_redshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_darkhair_blueshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_darkhair_greenshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_darkhair_purpleshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_darkhair_redshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_pinkhair_blueshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_pinkhair_greenshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_pinkhair_purpleshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_pinkhair_redshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_redhair_blueshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_redhair_greenshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_redhair_purpleshirt.png")
	path.append("res://player/charwalks/girl/charwalk_girl_redhair_redshirt.png")