extends Control

func _ready():
	# Called every time the node is added to the scene.
	gamestate.connect("connection_failed", self, "_on_connection_failed")
	gamestate.connect("connection_succeeded", self, "_on_connection_success")
	gamestate.connect("player_list_changed", self, "refresh_lobby")
	gamestate.connect("game_ended", self, "_on_game_ended")
	gamestate.connect("game_error", self, "_on_game_error")
	get_node("connect/name").grab_focus();
	# if the savegame exists, enable the continue button. otherwise, leave it disabled.
	#if(gamestate.check_savegame_exists()):
	#	get_node("connect/continue").show()

func _on_host_pressed():
	if (get_node("connect/name").text == ""):
		get_node("connect/error_label").text="Invalid name!"
		return

	get_node("connect").hide()
	get_node("players").show()
	get_node("connect/error_label").text=""

	var player_name = get_node("connect/name").text
	var player_skin = get_node("connect/char_sel").get_skin()
	gamestate.host_game(player_name, player_skin)
	refresh_lobby()

func _on_join_pressed():
	if (get_node("connect/name").text == ""):
		get_node("connect/error_label").text="Invalid name!"
		return

	var ip = get_node("connect/ip").text
	if (not ip.is_valid_ip_address()):
		get_node("connect/error_label").text="Invalid IPv4 address!"
		return

	get_node("connect/error_label").text=""
	get_node("connect/host").disabled=true
	get_node("connect/join").disabled=true

	var player_name = get_node("connect/name").text
	var player_skin = get_node("connect/char_sel").get_skin()
	gamestate.join_game(ip, player_name, player_skin)
	refresh_lobby() #gets called by the player_list_changed signal

func _on_connection_success():
	get_node("connect").hide()
	get_node("players").show()

func _on_connection_failed():
	get_node("connect/host").disabled=false
	get_node("connect/join").disabled=false
	get_node("connect/error_label").set_text("Connection failed.")

func _on_game_ended():
	show()
	get_node("connect").show()
	get_node("players").hide()
	get_node("connect/host").disabled=false
	get_node("connect/join").disabled

func _on_game_error(errtxt):
	get_node("error").dialog_text = errtxt
	get_node("error").popup_centered_minsize()

func refresh_lobby():
	var players = gamestate.get_player_list()
	players.sort()
	get_node("players/start").grab_focus();
	get_node("players/list").clear()
	get_node("players/list").add_item(gamestate.get_player_name() + " (You)")
	for p in players:
		get_node("players/list").add_item(p)

	get_node("players/start").disabled=not get_tree().is_network_server()

func _on_start_pressed():
	gamestate.begin_game()
#	rpc("load_skill_select")
#
#sync func load_skill_select():
#	var skill_pick = load("res://title_menu/Skills.tscn").instance()
#	get_tree().get_root().add_child(skill_pick)
#	var map = 	load("res://title_menu/Skills.tscn")
#	map = map.instance()
#	get_tree().get_root().get_node("world/StageMusic").stop();
#	get_tree().get_root().get_node("world").add_child(map)

func _on_continue_pressed():
	#savegame.load_game()
	# load the game and begin hosting
	get_node("players/save_name_list").show()
	if (get_node("connect/name").text == ""):
		get_node("connect/error_label").text="Invalid name!"
		return

	get_node("connect").hide()
	get_node("players").show()
	get_node("connect/error_label").text=""
	
	var player_name = get_node("connect/name").text
	var player_skin = get_node("connect/char_sel").get_skin()
	gamestate.host_game(player_name, player_skin)
	refresh_lobby()

#func _on_lobby_resized():
#	var current_size = OS.get_window_size()
#	var viewport = get_node("/root")
#	var height = viewport.get_rect().size
#	var width = viewport.get_rect().size
#	viewport.set_size_override(true, current_size)
#	viewport.set_size_override_stretch(true)


func _on_GoBackButton_pressed():
	gamestate.cleanup()
	self.queue_free()
	
func _on_GoBackButtonNoCleanup_pressed(): #This method causes crashes when cleanup() is called in tutorial
	#gamestate.cleanup()
	self.queue_free()

func _on_pressed():
	get_node("Selected").play();
