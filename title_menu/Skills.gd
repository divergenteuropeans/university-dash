extends CanvasLayer

# Object Primatives
var _ptsLabel
var _SATLabel
var _retakeSATLabel

# Skill Labels
var _creatLabel
var _logicLabel
var _commLabel
var _initLabel
var _humanLabel
var _vocatLabel

# Misc
var isShown = false
var timer
var time 	= 2
var player_SAT

var tree_root;
var current_scene;

var startingSkillPts

func _generate_SATscore():
	randomize()
	player_SAT = rand_range(1000, 1600)
	player_SAT = int(round(player_SAT))
	_SATLabel.set_text(str(player_SAT))

func _generate_SkillPts():
	if (player_SAT < 1100):
		startingSkillPts = 8
	elif (player_SAT < 1200):
		startingSkillPts = 9
	elif (player_SAT < 1300):
		startingSkillPts = 10
	elif (player_SAT < 1400):
		startingSkillPts = 11
	elif (player_SAT < 1490):
		startingSkillPts = 12
	elif (player_SAT < 1530):
		startingSkillPts = 13
	elif (player_SAT < 1560):
		startingSkillPts = 14
	elif (player_SAT >= 1560):
		startingSkillPts = 15
	_ptsLabel.set_text(str(startingSkillPts))

var skills
func resetSkills():
	skills = {
		"creativity" 	: 0,
		"logic"			: 0,
		"communication"	: 0,
		"initiative"	: 0,
		"humanity"		: 0,
		"vocational"	: 0,
		"unassigned"	: 0,
		"SAT"			: 1000,
		}

func _ready():
	resetSkills()
	
	get_node("VBoxContainer/CreativityButton").grab_focus();
	
	tree_root = get_tree().get_root();
	current_scene = tree_root.get_child(tree_root.get_child_count() -1);
	
	#gamestate.get_ClientPlayer().hud_hide()
	_SATLabel		= get_node("SATYellow/ScoreText/SATLabel")
	_retakeSATLabel	= get_node("RetakeSATButton/Label3")
	_ptsLabel		= get_node("PtsText/PtsLabel")
	_creatLabel 	= get_node("VBoxContainer/CreativityButton/Label")
	_logicLabel		= get_node("VBoxContainer/LogicButton/Label")
	_commLabel		= get_node("VBoxContainer/CommunicationButton/Label")
	_initLabel		= get_node("VBoxContainer/InitiativeButton/Label")
	_humanLabel 	= get_node("VBoxContainer/HumanitiesButton/Label")
	_vocatLabel		= get_node("VBoxContainer/VocationalButton/Label")
	
	_generate_SATscore()
	_generate_SkillPts()
	# Hide timer
	timer = Timer.new()
	timer.set_wait_time(time)
	timer.connect("timeout", self, "HideNote")
	add_child(timer)	#Add to Scene
	pass

## BUTTON SELECTIONS ##
func _on_CreatButton_mouse_entered():
	_creatLabel.set_text("Creativity +1?   (" + str(skills["creativity"]) + ")")
	#ShowNote()
func _on_CreatButton_mouse_exited():
	_creatLabel.set_text("Creativity   (" + str(skills["creativity"]) + ")")
	#timer.start()
func _on_LogicButton_mouse_entered():
	_logicLabel.set_text("Logic +1?   (" + str(skills["logic"]) + ")")
	#ShowNote()
func _on_LogicButton_mouse_exited():
	#timer.start()
	_logicLabel.set_text("Logic   (" + str(skills["logic"]) + ")")
func _on_CommButton_mouse_entered():
	_commLabel.set_text("Communication +1?   (" + str(skills["communication"]) + ")")
	#ShowNote()
func _on_CommButton_mouse_exited():
	#timer.start()
	_commLabel.set_text("Communication      (" + str(skills["communication"]) + ")")
func _on_InitButton_mouse_entered():
	_initLabel.set_text("Initiative +1?   (" + str(skills["initiative"]) + ")")
	#ShowNote()
func _on_InitButton_mouse_exited():
	#timer.start
	_initLabel.set_text("Initiative   (" + str(skills["initiative"]) + ")")
func _on_HumanButton_mouse_entered():
	_humanLabel.set_text("Humanities +1?   (" + str(skills["humanity"]) + ")")
	#ShowNote()
func _on_HumanButton_mouse_exited():
	#timer.start()
	_humanLabel.set_text("Humanities   (" + str(skills["humanity"]) + ")")
func _on_VocatButton_mouse_entered():
	_vocatLabel.set_text("Vocational +1?   (" + str(skills["vocational"]) + ")")
	#ShowNote()
func _on_VocatButton_mouse_exited():
	#timer.start()
	_vocatLabel.set_text("Vocational   (" + str(skills["vocational"]) + ")")


## INPUT EVENTS ## 
func _on_CreativityButton_pressed():
	_on_skillsbutton_pressed();
	if (startingSkillPts > 0):
		startingSkillPts -= 1
		skills["creativity"] += 1
		_ptsLabel.set_text(str(startingSkillPts))
		_on_CreatButton_mouse_entered()
func _on_LogicButton_pressed():
	_on_skillsbutton_pressed();
	if (startingSkillPts > 0):
		startingSkillPts -= 1
		skills["logic"] += 1
		_ptsLabel.set_text(str(startingSkillPts))
		_on_LogicButton_mouse_entered()
func _on_CommunicationButton_pressed():
	_on_skillsbutton_pressed();
	if (startingSkillPts > 0):
		startingSkillPts -= 1
		skills["communication"] += 1
		_ptsLabel.set_text(str(startingSkillPts))
		_on_CommButton_mouse_entered()	
func _on_InitiativeButton_pressed():
	_on_skillsbutton_pressed();
	if (startingSkillPts > 0):
		startingSkillPts -= 1
		skills["initiative"] += 1
		_ptsLabel.set_text(str(startingSkillPts))
		_on_InitButton_mouse_entered()
func _on_HumanitiesButton_pressed():
	_on_skillsbutton_pressed();
	if (startingSkillPts > 0):
		startingSkillPts -= 1
		skills["humanity"] += 1
		_ptsLabel.set_text(str(startingSkillPts))
		_on_HumanButton_mouse_entered()
func _on_VocationalButton_pressed():
	_on_skillsbutton_pressed();
	if (startingSkillPts > 0):
		startingSkillPts -= 1
		skills["vocational"] += 1
		_ptsLabel.set_text(str(startingSkillPts))
		_on_VocatButton_mouse_entered()
		

func _on_ConfirmButton_pressed():
	_on_pressed()
	#AudioStreamPlayer.stop()
#	get_node("Music").stop()
#	var tree_root = get_tree().get_root();
	#tree_root.remove_child(tree_root.get_child(tree_root.get_child_count() -1));
	#tree_root.remove_child(tree_root.get_child(tree_root.get_child_count() -3));
	#AudioStreamPlayer.stop()
	#skills["unassigned"] += startingSkillPts
	if(startingSkillPts == 0):
		gamestate.get_ClientPlayer().updateStats(skills)
		#if gamestate.get_ClientPlayer().isHudShown == false:
			#gamestate.get_ClientPlayer().hud_show()
		gamestate.get_ClientPlayer().hud_show()
		get_node("/root/world/StageMusic").play();
		self.queue_free()
	else:
		get_node("ConfirmButton/unspent_container").show()
	gamestate.in_world = true;


func retaken(object):
	get_tree().disconnect("node_removed", self, "retaken");
	get_node("Music").play();
	player_SAT = skills["SAT"];
	_SATLabel.set_text(str(player_SAT))
	_on_VocatButton_mouse_exited()
	_on_CreatButton_mouse_exited()
	_on_InitButton_mouse_exited()
	_on_HumanButton_mouse_exited()
	_on_CommButton_mouse_exited()
	_on_LogicButton_mouse_exited()
	_generate_SkillPts()

func _on_RetakeSATButton_pressed():
	_on_pressed()
	resetSkills()
	var puzzle = load("res://puzzles/Relentless/Relentless.tscn").instance();
	get_node("/root/world").add_child(puzzle);
	get_node("Music").stop();
	get_tree().connect("node_removed", self, "retaken");


# Reroll
func _on_RetakeSATButton_mouse_entered():
	_retakeSATLabel.set_text("Start Puzzle")
func _on_RetakeSATButton_mouse_exited():
	_retakeSATLabel.set_text("Retake SAT")


# Button Sounds
func _on_pressed():
	get_node("Select").play()
func _on_skillsbutton_pressed():
	get_node("SkillSelect").play();


func _on_ResetButton_pressed():
	resetSkills()
	_generate_SkillPts()
	skills["SAT"] = player_SAT
	update_Labels()

func update_Labels():
	_creatLabel.set_text("Creativity   (" + str(skills["creativity"]) + ")")
	_logicLabel.set_text("Logic   (" + str(skills["logic"]) + ")")
	_commLabel.set_text("Communication      (" + str(skills["communication"]) + ")")
	_initLabel.set_text("Initiative   (" + str(skills["initiative"]) + ")")
	_humanLabel.set_text("Humanities   (" + str(skills["humanity"]) + ")")
	_vocatLabel.set_text("Vocational   (" + str(skills["vocational"]) + ")")
	pass # replace with function body
