extends Sprite

var soundvol;
var musicvol;

func _ready():
	soundvol = get_node("OptionsPanel/VBoxContainer/SoundVol");
	musicvol = get_node("OptionsPanel/VBoxContainer/MusicVol");
	soundvol.value = (AudioServer.get_bus_volume_db(1)+50) * 2;
	musicvol.value = (AudioServer.get_bus_volume_db(2)+50) * 2;
	soundvol.grab_focus()
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _on_MusicVol_value_changed(value):
	var dbvol = (value/2)-50
	AudioServer.set_bus_volume_db(2, dbvol);

func _on_SoundVol_value_changed(value):
	var dbvol = (value/2)-50
	AudioServer.set_bus_volume_db(1, dbvol);


func _on_Button_pressed():
	var tree_root = get_tree().get_root();
	
	if(tree_root.get_node("MainMenu") == null):
		get_tree().change_scene("res://title_menu/MainMenu.tscn");
		#tree_root.get_child(tree_root.get_child_count() -2).free()
		tree_root.remove_child(tree_root.get_child(tree_root.get_child_count() -2));
	else:
		#tree_root.get_child(tree_root.get_child_count() -1).free()
		tree_root.get_node("MainMenu/VBoxContainer/OptionsButton").grab_focus();
		tree_root.remove_child(tree_root.get_child(tree_root.get_child_count() -1));
	pass # replace with function body

func on_laptopmode(onoff):
	OS.set_low_processor_usage_mode(onoff)
