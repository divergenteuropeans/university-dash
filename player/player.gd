extends KinematicBody2D

slave var slave_pos = Vector2()
slave var slave_inert = Vector2()
slave var skin

## Move Vars ##
const IU = 0.5				## Impulse Unit
const MAX_SPEED = 2			## Max ImpulseUnits
var BASE_MULTIPLIER = 200.0	## Amplifies base IUs
var move_multiplier = 1.1		## Movement Multi.
var friction = .25			## Decays base IUs
var inert = Vector2(0,0)		## IU Vector
var console = preload("res://console/Console.tscn").instance()

# Boolean signals to let other GUI elements work 
# without Godot syncing innaccuracies
var isPuzzleShown = false # This should preferably be implemented to each puzzle's start event method.
var isHudShown = false
var isPlayerGraduated = false # When player graduates, variable is set to true, which disables access to puzzles and NPCs.
onready var score = 0
var buff = 0
var hud;
func hud_hide():
	get_node("hud/Panel").hide()
	get_node("itemMenu/ViewportContainer").hide()
	isHudShown = false
func hud_show():
	get_node("hud/Panel").show()
	get_node("itemMenu/ViewportContainer").show()
	isHudShown = true

var skills = {
	"creativity" 	: 0,
	"logic"			: 0,
	"communication"	: 0,
	"initiative"	: 0,
	"humanity"		: 0,
	"vocational"	: 0,
	"unassigned"	: 0,
	}

var vitals = {
	"health"		: 100,
	"sanity"		: 100
	}
	
var GPA = 0

export var stunned = true

# Use sync because it will be called everywhere
sync func setup_bomb(bomb_name, pos, by_who):
#	var bomb = preload("res://player/player_props/bomb.tscn").instance()
#	bomb.set_name(bomb_name) # Ensure unique name for the bomb
#	bomb.position=pos
#	bomb.from_player = by_who
#	# No need to set network mode to bomb, will be owned by master by default
#	get_node("../..").add_child(bomb)
	pass

var current_anim = ""
var prev_bombing = false
var bomb_index = 0
var num_classes = 0;

func dampen():
		if inert.x > 0:
			inert.x -= friction
		if inert.x < 0:
			inert.x += friction
		if inert.y > 0:
			inert.y -= friction
		if inert.y < 0:
			inert.y += friction


func _physics_process(delta):
	
	if (is_network_master() && gamestate.in_world):

		## MOVEMENT ##
		if (Input.is_action_pressed("move_left")):
			if -inert.x < MAX_SPEED:
				inert.x -= IU
		if (Input.is_action_pressed("move_right")):
			if inert.x < MAX_SPEED:
				inert.x += IU
		if (Input.is_action_pressed("move_up")):
			if -inert.y < MAX_SPEED:
				inert.y -= IU
		if (Input.is_action_pressed("move_down")):
			if inert.y < MAX_SPEED:
				inert.y += IU

		var bombing = Input.is_action_pressed("set_bomb")

		if (stunned):
			bombing = false
			inert = Vector2()

#		if (bombing and not prev_bombing):
#			var bomb_name = get_name() + str(bomb_index)
#			var bomb_pos = position
#			rpc("setup_bomb", bomb_name, bomb_pos, get_tree().get_network_unique_id())

		prev_bombing = bombing

		rset("slave_inert", inert)
		rset("slave_pos", position)
	else:
		position=slave_pos
		inert = slave_inert

	var new_anim = "standing"
	if (inert.y < 0):
		new_anim = "walk_up"
	elif (inert.y > 0):
		new_anim = "walk_down"
	elif (inert.x < 0):
		new_anim = "walk_left"
	elif (inert.x > 0):
		new_anim = "walk_right"
	elif (inert.x == 0 && inert.y == 0):
		new_anim = "standing"

	if (stunned):
		new_anim = "stunned"

	if (new_anim != current_anim):
		current_anim = new_anim
		get_node("anim").play(current_anim)

	dampen()
	move_and_slide(inert*BASE_MULTIPLIER*move_multiplier)
	if (not is_network_master()):
		slave_pos = position # To avoid jitter

slave func stun():
	stunned = true

master func exploded(by_who):
	if (stunned):
		return
	rpc("stun") # Stun slaves
	stun() # Stun master - could use sync to do both at once

func set_player_name(new_name):
	get_node("Control/label").set_text(new_name)
	
func set_player_skin(new_skin):
	get_node("sprite").set_texture(load(new_skin))
	skin = new_skin	

func htimer():
	if(vitals["health"] > 0):
		updateVitals({
			health = -1,
			sanity = 0,
		});
	else:
		updateVitals({
			health = 0,
			sanity = -2,
		});

func _ready():
	#updateStats(self.skills)
	stunned = false
	slave_pos = position
	hud = get_node("hud")
	hud_hide()
	get_node("HealthTimer").start();


## RPC is weird...
func openBubble(msg):
	rpc ("syncBubble", msg)
sync func syncBubble(msg):
	get_node("Control").openBubble(msg)

# This is also the same as gamestate.get_ClientPlayer().skills.
func getStats():
	return skills

# This is the main method that updates the player's skills dict.
# Prec: Takes in a skills dict.
# Postc: *Adds* to not overrides the existing player's skills dict.
# 	For ex. a skills dict with a value in one of the skills categories as
# 	-1 will decrease 1 of the categories.
sync func updateStats(skills):		
	self.skills["creativity"]		+= skills["creativity"]
	self.skills["logic"]			+= skills["logic"]
	self.skills["communication"]	+= skills["communication"]
	self.skills["initiative"]		+= skills["initiative"]
	self.skills["humanity"]			+= skills["humanity"]
	self.skills["vocational"]		+= skills["vocational"]
	self.skills["unassigned"]		+=skills["unassigned"]
	Console.scoreUpdate(get_tree().get_network_unique_id(), gamestate.get_player_name(), self.skills, score)	#ID, Name, Skill

	hud.updateStats(self.skills);
	get_node("hud").num_classes = num_classes;

sync func updateClasses(classdelta):
	num_classes += classdelta;

func updateVitals(vitals):		# Add Vitals to Existing Vitals
	# clamp vitals between 0-100
	self.vitals["health"]	+= max(0-self.vitals["health"], min(vitals["health"], 100 - self.vitals["health"]));
	self.vitals["sanity"]	+= max(0-self.vitals["sanity"], min(vitals["sanity"], 100 - self.vitals["sanity"]));
	if(self.vitals["health"] > 90):
		move_multiplier = 1.1
	else:
		move_multiplier = float(self.vitals["health"]) / 100 + 0.10
#	print(move_multiplier)
	if(move_multiplier < 0.5):
		move_multiplier = 0.5
	move_multiplier += buff
	get_node("hud").show_movement(move_multiplier);
#	print(buff)
#	print(move_multiplier)
	get_node("hud").show_vitals(self.vitals)
	
func updateMovemuli(val):
	move_multiplier += val
	get_node("hud").show_movement(move_multiplier);

func updateDay(day):
	get_node("hud").updateDay(day)
func updateStanding(grade):
	get_node("hud").updateStanding(grade)
	
func hide_inventory():
	get_node("itemMenu").hide()

func update_buff(modifier):
	buff += modifier
	var sentValue = {
		"health":0,
		"sanity":0
		}
	updateVitals(sentValue)
