extends CanvasLayer

# Object Primatives
onready	var world = get_node("/root/world")
var _panel
var _barhealth
var _barsanity
var _labelspeed
var _labeltime
var _iconphase

# Stat Labels
var _stat_cre
var _stat_log
var _stat_com
var _stat_ini
var _stat_hum
var _stat_voc
var _stat_unu;
var _standing
var _days
var _gpa
var _score

#Day phase timer value.
var time;

var initscore = -1;

var num_classes = 0;

var phase = {
	"Day":"res://player/hud/phase_icon_day.svg",
	"Evening":"res://player/hud/phase_icon_evening.svg",
	"Night":"res://player/hud/phase_icon_night.svg",
	}

func _ready():
	_panel = get_node("Panel")
	_barhealth = get_node("Panel/Condition/health")
	_barsanity = get_node("Panel/Condition/sanity")
	_labelspeed= get_node("Panel/Movement/speed")
	_labeltime = get_node("Panel/Time/timer")
	_iconphase = get_node("Panel/Time/phase_icon")
	# Label
	_stat_cre = get_node("Stats_Panel/HBoxContainer/Stats#/Creativity")
	_stat_log = get_node("Stats_Panel/HBoxContainer/Stats#/Logic")
	_stat_com = get_node("Stats_Panel/HBoxContainer/Stats#/Communication")
	_stat_ini = get_node("Stats_Panel/HBoxContainer/Stats#/Initiative")
	_stat_hum = get_node("Stats_Panel/HBoxContainer/Stats#/Humanity")
	_stat_voc = get_node("Stats_Panel/HBoxContainer/Stats#/Vocational")
	_stat_unu = get_node("Stats_Panel/HBoxContainer/Session#/Skills");
	_standing = get_node("Stats_Panel/HBoxContainer/Session#/Standing")
	_days = 	get_node("Stats_Panel/HBoxContainer/Session#/Day")
	_score =	get_node("Stats_Panel/HBoxContainer/Session#/ScoreNum")
	_gpa =		get_node("Stats_Panel/HBoxContainer/Session#/Grade")

var just_pressed_statsbutton = false;

func _process(delta):
	if Input.is_action_pressed("Stats_View") && gamestate.in_world == true:
		_on_Stats_Button_pressed();
	show_time()


## Variables are stored in player and passed to HUD
func show_vitals(vitals):
	_barhealth.set_value(vitals["health"])
	_barsanity.set_value(vitals["sanity"])

func show_movement(speed):
	_labelspeed.set_text(str(speed)+"x")

func show_time():
	time         = int(world.get_time())
	var secondstring = "";
	var minutes      = (time / 60)
	var seconds      = (time % 60)
	if (seconds < 10):
		seconds = "0" + str(seconds)
	var texTime = str(minutes) + ":" + str(seconds)
	_labeltime.set_text(texTime)
	update_phase()
func update_phase():
	var name = world.get_phase()
	_iconphase.set_texture(load(phase[name]))

# These methods control when the HUD shows up or not.
func _on_Stats_Button_pressed():
	updateStats(gamestate.get_ClientPlayer().skills) #This had to be included for bugs where sometimes the HUD wouldn't update properly in multiplayer.
	get_node("Stats_Panel").show()
	get_node("Stats_Panel/Button").grab_focus();
func _on_Button_pressed():
	get_node("Stats_Panel").hide()

func updateStats(stats):
	_stat_cre.text = String(stats["creativity"])
	_stat_log.text = String(stats["logic"] )
	_stat_com.text = String(stats["communication"])
	_stat_ini.text = String(stats["initiative"])
	_stat_hum.text = String(stats["humanity"])
	_stat_voc.text = String(stats["vocational"])
	_stat_unu.text = String(stats["unassigned"])
	
	var score = 0
	for n in stats.values():	# Manually sum Skills earned
		score += n
	if (initscore == -1 && score != 0):
		initscore = score
	_score.set_text(str(score * 1000))
	
	### Ykaro's GPA code has been commented out for now, since the GPA does not update properly in multiplayer.
	### This an algorithm based on calculating GPA on how many classes have been taken and the total score achieved.
	### Use "Ctrl + K" to mass comment/uncomment so we can get this working later.

#	var gpaval = 0;
#	print("Score: " + str(score))
#	print("N_classes: " + str(num_classes))
#	print("Initscore: " + str(initscore));
#	if(num_classes > 0):
#		gpaval = (score - initscore) / (num_classes * 10);
#		gpaval *= 4;
#	else:
#		gpaval = 4.0;

	# We can later scale these values if we decide to implement a game speed/game difficulty option in lobby or game settings.
	#var scores = {} # in format p_id : score
	#var playerlist = gamestate.players
	if (score < 20):
		updateStanding("Freshman")
	elif (score < 36):
		updateStanding("Sophmore")
	elif (score < 52):
		updateStanding("Junior")
	elif (score >= 68):
		updateStanding("Senior")
	var strGPA = ("%1.2f" % gamestate.get_ClientPlayer().GPA)
	_gpa.text = strGPA

	
func updateDay(day):
	_days.set_text(String(day))
func updateStanding(grade):
	_standing.set_text(grade)
